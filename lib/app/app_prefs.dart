//import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static late GetStorage _storagePreferences;

  static final AppPreferences _appPref = AppPreferences._internal();

  static Future<AppPreferences> init() async {
    //WidgetsFlutterBinding.ensureInitialized();
    _storagePreferences = GetStorage();
   // await SharedPreferences.getInstance();
    //_sharedPreferences.clear();
    return AppPreferences();
  }

  factory AppPreferences() {
    return _appPref;
  }

  AppPreferences._internal();

  final String _PREFS_KEY_IS_USER_LOGGED_IN = "PREFS_KEY_IS_USER_LOGGED_IN";
  final String _PREFS_KEY_ACCESS_TOKEN = "_PREFS_KEY_ACCESS_TOKEN";
  final String _PREFS_KEY_Encrypted_Access_Token_ACCESS_TOKEN =
      "_PREFS_KEY_Encrypted_Access_Token_ACCESS_TOKEN";
  final String _PREFS_KEY_Expire_In_Seconds = "_PREFS_KEY_Expire_In_Seconds";
  final String _PREFS_KEY_User_Id = "_PREFS_KEY_User_Id";

  //todo::We should make a form for declaration
  final String _PREFS_KEY_USER_DATA = "_PREFS_KEY_USER_DATA";

  //login

  Future<void> setUserLoggedIn() async {
    _storagePreferences.write(_PREFS_KEY_IS_USER_LOGGED_IN, true);
  }

  bool isUserLoggedIn() {
    return _storagePreferences.read(_PREFS_KEY_IS_USER_LOGGED_IN) ?? false;
  }

  Future<void> logout() async {
    _storagePreferences.remove(_PREFS_KEY_IS_USER_LOGGED_IN);
  }

  Future<void> setAccessToken(String token) async {
    _storagePreferences.write(_PREFS_KEY_ACCESS_TOKEN, token);
  }

  Future<void> setEncryptedAccessToken(String token) async {
    _storagePreferences.write(
        _PREFS_KEY_Encrypted_Access_Token_ACCESS_TOKEN, token);
  }

  Future<void> setExpireInSeconds(int token) async {
    _storagePreferences.write(_PREFS_KEY_Expire_In_Seconds, token);
  }
  Future<void> setUserId(int token) async {
    _storagePreferences.write(_PREFS_KEY_User_Id, token);
  }

  Future<void> removeToken() async {
    _storagePreferences.remove(_PREFS_KEY_ACCESS_TOKEN);
  }

  Future<String?> getAccessToken() async {
    return _storagePreferences.read(_PREFS_KEY_ACCESS_TOKEN);
  }

  Future<String?> getEncryptedAccessToken() async {
    return _storagePreferences
        .read(_PREFS_KEY_Encrypted_Access_Token_ACCESS_TOKEN);
  }

  Future<String?> getExpireInSeconds() async {
    return _storagePreferences.read(_PREFS_KEY_Expire_In_Seconds);
  }

  Future<String?> getUserId() async {
    return _storagePreferences
        .read(_PREFS_KEY_User_Id);
  }

  Future<void> setUser(dynamic user) async {
    _storagePreferences.write(_PREFS_KEY_USER_DATA, user);
  }

  Future<dynamic> getUser() async {
    return _storagePreferences.read(_PREFS_KEY_USER_DATA);
  }
}
