import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/home/home_binding.dart';
import 'package:job_application_test/view/login/login_binding.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../view/resources/routes_manager.dart';
import '../view/resources/theme_manager.dart';
import '../view/splash/splash_binding.dart';

class MyApp extends StatelessWidget {

  //final prefs = await SharedPreferences.getInstance();
  MyApp._internal(); // private named constructor
  int appState = 0;
  static final MyApp instance =
  MyApp._internal(); // single instance -- singleton

  factory MyApp() => instance; // factory for the class instance

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      initialBinding: SplashBinding(),
      initialRoute: Routes.splashRoute,
      theme: getLightTheme(),
    );
  }
}