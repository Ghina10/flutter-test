import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../data/data_source/local_data_source.dart';
import '../data/data_source/remote_data_source.dart';
import '../data/network/app_api_Implmentation.dart';
import '../data/network/network_info.dart';
import '../data/repository/repository_impl.dart';
import '../domain/repository/repository.dart';

class NoteAppDependency {
  static final NoteAppDependency _instance = NoteAppDependency._internal();

  factory NoteAppDependency() {
    return _instance;
  }

  NoteAppDependency._internal();

  NoteAppDependency get dependences => _instance;

  Repository get repo => _repository;
  late final Repository _repository = RepositoryImpl(_remoteDs, _netInfo, _localDs);
  late final RemoteDataSource _remoteDs = RemoteDataSourceImpl(_appService);
  late final LocalDataSource _localDs = LocalDataSourceImpl();
  late final NetworkInfo _netInfo = NetworkInfoImpl(InternetConnectionChecker());

  late final AppServiceClient _appService = AppServiceClient(getDioWithInterceptor());

  static Dio getDioWithInterceptor() {
    Dio dio = Dio();
    dio.interceptors.add(dioLoggerInterceptor);
    return dio;
  }
}
