bool isEmail_Valid(String email) {
  return RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
}

bool isPassword_Valid(String password) {
  return password.length >= 8 ? true : false;
}

bool isConfirmPassword_Valid(String password, String confirmPassword) {
  return password == confirmPassword;
}
