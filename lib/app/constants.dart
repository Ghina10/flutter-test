//flutter pub run build_runner build
class Constants {
  static const String empty = "";
  static const int zero = 0;
}

class AppUrls {
  static const String baseUrl = "https://mobiletest.tuqaatech.com";
  static const String loginUrl = baseUrl + '/api/TokenAuth/Authenticate';
  static const String registerUrl =
      baseUrl + '/api/services/app/Account/Register';
  static const String allCityUrl =
      baseUrl + '/api/services/app/City/GetAllCities';
  static const String allCountryUrl =
      baseUrl + '/api/services/app/Country/GetAllCountries';
  static const String downloadImageUrl =
      baseUrl + '/api/services/app/UserInformation/DownloadImage';
  static const String getAllCityPartnersUrl =
      baseUrl + '/api/services/app/CityPartner/GetAllCityPartners';
  static const String getAllChatListUrl =
      baseUrl + '/api/services/app/Chat/GetAllChatList';
}
