import '../../app/constants.dart';
import '../../domain/model/models.dart';
import '../../domain/model/user.dart';
import '../response/responses.dart';

extension UserResponseMapper on UserDataResponse? {
  User toDomain() {
    return User(
        this?.id ?? Constants.empty,
        this?.userName ?? Constants.empty,
        this?.name ?? Constants.empty,
        this?.surname ?? Constants.empty,
        this?.emailAddress ?? Constants.empty,
        this?.isActive ?? Constants.empty,
        this?.fullName ?? Constants.empty,
        this?.lastLoginTime ?? Constants.empty,
        this?.creationTime ?? Constants.empty,
        this?.roleNames ?? Constants.empty);
  }
}

extension UserRegisterResponseMapper on UserRegisterResponse {
  bool toDomain() {
    return result!.toDomain();
  }
}

extension RegisterDataResponseMapper on UserRegisterDataResponse {
  bool toDomain() {
    return canLogin!;
  }
}

extension UserLoginResponseMapper on UserLoginResponse {
  LoginInfo toDomain() {
    return result!.toDomain();
  }
}

extension LoginDataResponseMapper on UserLoginDataResponse {
  LoginInfo toDomain() {
    return LoginInfo(
        accessToken, encryptedAccessToken, expireInSeconds, userId);
  }
}

extension CityMapper on CityResponse {
  City toDomain() {
    return City(countryId!, countryName!, name!, id!);
  }
}

extension GetAllCitiesMapper on GetAllCitiesResponse {
  List<City> toDomain() {
    List<City> _cities = [];
    result!.forEach((element) {
      _cities.add(element.toDomain());
    });

    return _cities;
  }
}

extension CountryMapper on CountryResponse {
  Country toDomain() {
    List<City> _cities = [];
    cities!.forEach((element) {
      _cities.add(element.toDomain());
    });
    return Country(name!, _cities);
  }
}

extension GetCountriesMapper on GetAllCountriesResponse {
  List<Country> toDomain() {
    List<Country> _countries = [];
    result!.forEach((element) {
      _countries.add(element.toDomain());
    });
    return _countries;
  }
}

extension GetAllCityPartnersMapper on GetAllCityPartnersResponse {
  List<GetAllCityPartners> toDomain() {
    List<GetAllCityPartners> _partners = [];
    result!.forEach((element) {
      _partners.add(element.toDomain());
    });
    return _partners;
  }
}

extension GetAllCityPartnersDataMapper on CityPartnersResponse {
  GetAllCityPartners toDomain() {
    return GetAllCityPartners(
        id!, prtnerId!, cityId!, age!, gender!, date!, userName!, cityName!);
  }
}

extension DownloadImageMapper on DownloadImageResponse {
  String toDomain() {
    return result!;
  }
}

extension GetAllChatListMapper on GetAllChatListResponse {
  List<GetAllChatList> toDomain() {
    List<GetAllChatList> _chates = [];
    result!.forEach((element) {
      _chates.add(element.toDomain());
    });
    return _chates;
  }
}

extension GetAllChatListDataMapper on GetAllChatListDataResponse {
  GetAllChatList toDomain() {
    return GetAllChatList(id!, accountId!, fullName!, contactId!, lastMessage!,
        lastMessageTime!, dialogs!.toDomain());
  }
}

extension DialogMapper on DialogsResponse {
  Dialog toDomain() {
    return Dialog(id!, who!, message!, chatid!, time!);
  }
}
