import '../network/app_api_Implmentation.dart';
import '../network/requests.dart';
import '../response/responses.dart';

abstract class RemoteDataSource {
  Future<UserLoginResponse> login(LoginRequest loginRequest);

  Future<UserRegisterResponse> register(RegisterRequest registerRequest);

  Future<GetAllCitiesResponse> gatAllCities();

  Future<GetAllCountriesResponse> gatAllCountries();

  Future<GetAllCityPartnersResponse> getAllCityPartners(
      GetAllCityPartnersRequest getAllCityPartnersRequest);

  Future<DownloadImageResponse> downloadImage(int id);

  Future<GetAllChatListResponse> getAllChatList();
}

class RemoteDataSourceImpl implements RemoteDataSource {
  final AppServiceClient _appServiceClient;

  RemoteDataSourceImpl(this._appServiceClient);

  @override
  Future<UserLoginResponse> login(LoginRequest loginRequest) async {
    //print("remoteData:${loginRequest.email} ${loginRequest.password}");
    return await _appServiceClient.login(
        loginRequest.email, loginRequest.password, loginRequest.rememberClient);
  }

  @override
  Future<UserRegisterResponse> register(RegisterRequest registerRequest) async {
    return await _appServiceClient.register(
        registerRequest.name,
        registerRequest.gender,
        registerRequest.age,
        registerRequest.cityId,
        registerRequest.countryId,
        registerRequest.phone,
        registerRequest.email,
        registerRequest.password,
        registerRequest.avatar);
  }

  @override
  Future<GetAllCitiesResponse> gatAllCities() async {
    return await _appServiceClient.gatAllCities();
  }

  @override
  Future<GetAllCountriesResponse> gatAllCountries() async {
    return await _appServiceClient.gatAllCountries();
  }

  @override
  Future<GetAllCityPartnersResponse> getAllCityPartners(
      GetAllCityPartnersRequest getAllCityPartnersRequest) async {
    return await _appServiceClient.getAllCityPartners(
        getAllCityPartnersRequest.cityId,
        getAllCityPartnersRequest.CountryId,
        getAllCityPartnersRequest.Gender,
        getAllCityPartnersRequest.MinAge,
        getAllCityPartnersRequest.MaxAge,
        getAllCityPartnersRequest.Date,
        getAllCityPartnersRequest.SkipCount,
        getAllCityPartnersRequest.MaxResultCount);
  }

  @override
  Future<DownloadImageResponse> downloadImage(int id) async{
    return await _appServiceClient.downloadImage(id);
  }

  @override
  Future<GetAllChatListResponse> getAllChatList() async {
    return await _appServiceClient.getAllChatList();
  }
}
