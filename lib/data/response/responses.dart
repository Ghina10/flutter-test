import 'package:json_annotation/json_annotation.dart';

part 'responses.g.dart';

@JsonSerializable()
class BaseResponse {
  @JsonKey(name: "targetUrl")
  String? targetUrl;
  @JsonKey(name: "success")
  bool? success;
  @JsonKey(name: "error")
  ErrorResponse? error;
  @JsonKey(name: "unAuthorizedRequest")
  bool? unAuthorizedRequest;
  @JsonKey(name: "__abp")
  bool? abp;
}

@JsonSerializable()
class ErrorResponse {
  @JsonKey(name: "code")
  int? code;
  @JsonKey(name: "message")
  String? message;
  @JsonKey(name: "details")
  String? details;
  @JsonKey(name: "validationErrors")
  List<ValidationErrorsResponse>? validationErrors;

  ErrorResponse(this.code, this.message, this.details, this.validationErrors);

  // from json
  factory ErrorResponse.fromJson(Map<String, dynamic> json) =>
      _$ErrorResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$ErrorResponseToJson(this);
}

@JsonSerializable()
class ValidationErrorsResponse {
  @JsonKey(name: "message")
  String? message;
  @JsonKey(name: "members")
  List<String>? members;

  ValidationErrorsResponse(this.message, this.members);

  // from json
  factory ValidationErrorsResponse.fromJson(Map<String, dynamic> json) =>
      _$ValidationErrorsResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$ValidationErrorsResponseToJson(this);
}

@JsonSerializable()
class UserDataResponse {
  @JsonKey(name: "id")
  String id;
  @JsonKey(name: "userName")
  String userName;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "surname")
  String surname;
  @JsonKey(name: "emailAddress")
  String emailAddress;
  @JsonKey(name: "isActive")
  String isActive;
  @JsonKey(name: "fullName")
  String fullName;
  @JsonKey(name: "lastLoginTime")
  String lastLoginTime;
  @JsonKey(name: "creationTime")
  String creationTime;
  @JsonKey(name: "roleNames")
  String roleNames;

  UserDataResponse(
      this.id,
      this.userName,
      this.name,
      this.surname,
      this.emailAddress,
      this.isActive,
      this.fullName,
      this.lastLoginTime,
      this.creationTime,
      this.roleNames);

// from json
  factory UserDataResponse.fromJson(Map<String, dynamic> json) =>
      _$UserDataResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$UserDataResponseToJson(this);
}

@JsonSerializable()
class UserLoginResponse extends BaseResponse {
  @JsonKey(name: "result")
  UserLoginDataResponse? result;

  UserLoginResponse(this.result);

  // from json
  factory UserLoginResponse.fromJson(Map<String, dynamic> json) =>
      _$UserLoginResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$UserLoginResponseToJson(this);
}

@JsonSerializable()
class UserLoginDataResponse {
  @JsonKey(name: "accessToken")
  String? accessToken;
  @JsonKey(name: "encryptedAccessToken")
  String? encryptedAccessToken;
  @JsonKey(name: "expireInSeconds")
  int? expireInSeconds;
  @JsonKey(name: "userId")
  int? userId;

  UserLoginDataResponse(this.accessToken, this.encryptedAccessToken,
      this.expireInSeconds, this.userId);

  factory UserLoginDataResponse.fromJson(Map<String, dynamic> json) =>
      _$UserLoginDataResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$UserLoginDataResponseToJson(this);
}

@JsonSerializable()
class UserRegisterResponse extends BaseResponse {
  @JsonKey(name: "result")
  UserRegisterDataResponse? result;

  UserRegisterResponse(this.result);

  factory UserRegisterResponse.fromJson(Map<String, dynamic> json) =>
      _$UserRegisterResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$UserRegisterResponseToJson(this);
}

@JsonSerializable()
class UserRegisterDataResponse {
  @JsonKey(name: "canLogin")
  bool? canLogin;

  UserRegisterDataResponse(this.canLogin);

  // from json
  factory UserRegisterDataResponse.fromJson(Map<String, dynamic> json) =>
      _$UserRegisterDataResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$UserRegisterDataResponseToJson(this);
}

@JsonSerializable()
class GetAllCitiesResponse extends BaseResponse{
  @JsonKey(name: "result")
  List<CityResponse>? result;

  // from json
  factory GetAllCitiesResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllCitiesResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$GetAllCitiesResponseToJson(this);

  GetAllCitiesResponse(this.result);
}

@JsonSerializable()
class CityResponse {
  @JsonKey(name: "countryId")
  int? countryId;
  @JsonKey(name: "countryName")
  String? countryName;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "id")
  int? id;

  // from json
  factory CityResponse.fromJson(Map<String, dynamic> json) =>
      _$CityResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$CityResponseToJson(this);

  CityResponse(this.countryId, this.countryName, this.name, this.id);
}

@JsonSerializable()
class GetAllCountriesResponse extends BaseResponse{
  @JsonKey(name: "result")
  List<CountryResponse>? result;

  // from json
  factory GetAllCountriesResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllCountriesResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$GetAllCountriesResponseToJson(this);

  GetAllCountriesResponse(this.result);
}

@JsonSerializable()
class CountryResponse {
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "cities")
  List<CityResponse>? cities;

  // from json
  factory CountryResponse.fromJson(Map<String, dynamic> json) =>
      _$CountryResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$CountryResponseToJson(this);

  CountryResponse(this.name, this.cities);
}


@JsonSerializable()
class GetAllCityPartnersResponse extends BaseResponse {
  @JsonKey(name: "result")
  List<CityPartnersResponse>? result;

  GetAllCityPartnersResponse(this.result);

  // from json
  factory GetAllCityPartnersResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllCityPartnersResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$GetAllCityPartnersResponseToJson(this);
}


@JsonSerializable()
class CityPartnersResponse{
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "prtnerId")
  int? prtnerId;
  @JsonKey(name: "cityId")
  int? cityId;
  @JsonKey(name: "age")
  int? age;
  @JsonKey(name: "gender")
  int? gender;
  @JsonKey(name: "date")
  DateTime? date;
  @JsonKey(name: "userName")
  String? userName;
  @JsonKey(name: "cityName")
  String? cityName;

  // from json
  factory CityPartnersResponse.fromJson(Map<String, dynamic> json) =>
      _$CityPartnersResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$CityPartnersResponseToJson(this);

  CityPartnersResponse(this.id, this.prtnerId, this.cityId, this.age,
      this.gender, this.date, this.userName, this.cityName);
}

@JsonSerializable()
class DownloadImageResponse extends BaseResponse{
  @JsonKey(name: "result")
  String? result;

  // from json
  factory DownloadImageResponse.fromJson(Map<String, dynamic> json) =>
      _$DownloadImageResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$DownloadImageResponseToJson(this);

  DownloadImageResponse(this.result);
}


@JsonSerializable()
class GetAllChatListResponse extends BaseResponse {
  @JsonKey(name: "result")
  List<GetAllChatListDataResponse>? result;

  GetAllChatListResponse(this.result);

  // from json
  factory GetAllChatListResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllChatListResponseFromJson(json);

// to json
  Map<String, dynamic> toJson() => _$GetAllChatListResponseToJson(this);
}

@JsonSerializable()
class GetAllChatListDataResponse {
  @JsonKey(name: "id")
  String? id;
  @JsonKey(name: "accountId")
  int? accountId;
  @JsonKey(name: "fullName")
  String? fullName;
  @JsonKey(name: "contactId")
  int? contactId;
  @JsonKey(name: "lastMessage")
  String? lastMessage;
  @JsonKey(name: "lastMessageTime")
  DateTime? lastMessageTime;
  @JsonKey(name: "dialogs")
  DialogsResponse? dialogs;


  // from json
  factory GetAllChatListDataResponse.fromJson(Map<String, dynamic> json) =>
      _$GetAllChatListDataResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$GetAllChatListDataResponseToJson(this);

  GetAllChatListDataResponse(this.id, this.accountId, this.fullName, this.contactId,
      this.lastMessage, this.lastMessageTime, this.dialogs);
}


@JsonSerializable()
class DialogsResponse{
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "who")
  int? who;
  @JsonKey(name: "message")
  String? message;
  @JsonKey(name: "chatid")
  int? chatid;
  @JsonKey(name: "time")
  DateTime? time;


  // from json
  factory DialogsResponse.fromJson(Map<String, dynamic> json) =>
      _$DialogsResponseFromJson(json);

  // to json
  Map<String, dynamic> toJson() => _$DialogsResponseToJson(this);

  DialogsResponse(this.id, this.who, this.message, this.chatid, this.time);
}

