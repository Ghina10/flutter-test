// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'responses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BaseResponse _$BaseResponseFromJson(Map<String, dynamic> json) => BaseResponse()
  ..targetUrl = json['targetUrl'] as String?
  ..success = json['success'] as bool?
  ..error = json['error'] == null
      ? null
      : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
  ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
  ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$BaseResponseToJson(BaseResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
    };

ErrorResponse _$ErrorResponseFromJson(Map<String, dynamic> json) =>
    ErrorResponse(
      json['code'] as int?,
      json['message'] as String?,
      json['details'] as String?,
      (json['validationErrors'] as List<dynamic>?)
          ?.map((e) =>
              ValidationErrorsResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ErrorResponseToJson(ErrorResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'details': instance.details,
      'validationErrors': instance.validationErrors,
    };

ValidationErrorsResponse _$ValidationErrorsResponseFromJson(
        Map<String, dynamic> json) =>
    ValidationErrorsResponse(
      json['message'] as String?,
      (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ValidationErrorsResponseToJson(
        ValidationErrorsResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'members': instance.members,
    };

UserDataResponse _$UserDataResponseFromJson(Map<String, dynamic> json) =>
    UserDataResponse(
      json['id'] as String,
      json['userName'] as String,
      json['name'] as String,
      json['surname'] as String,
      json['emailAddress'] as String,
      json['isActive'] as String,
      json['fullName'] as String,
      json['lastLoginTime'] as String,
      json['creationTime'] as String,
      json['roleNames'] as String,
    );

Map<String, dynamic> _$UserDataResponseToJson(UserDataResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'name': instance.name,
      'surname': instance.surname,
      'emailAddress': instance.emailAddress,
      'isActive': instance.isActive,
      'fullName': instance.fullName,
      'lastLoginTime': instance.lastLoginTime,
      'creationTime': instance.creationTime,
      'roleNames': instance.roleNames,
    };

UserLoginResponse _$UserLoginResponseFromJson(Map<String, dynamic> json) =>
    UserLoginResponse(
      json['result'] == null
          ? null
          : UserLoginDataResponse.fromJson(
              json['result'] as Map<String, dynamic>),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$UserLoginResponseToJson(UserLoginResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

UserLoginDataResponse _$UserLoginDataResponseFromJson(
        Map<String, dynamic> json) =>
    UserLoginDataResponse(
      json['accessToken'] as String?,
      json['encryptedAccessToken'] as String?,
      json['expireInSeconds'] as int?,
      json['userId'] as int?,
    );

Map<String, dynamic> _$UserLoginDataResponseToJson(
        UserLoginDataResponse instance) =>
    <String, dynamic>{
      'accessToken': instance.accessToken,
      'encryptedAccessToken': instance.encryptedAccessToken,
      'expireInSeconds': instance.expireInSeconds,
      'userId': instance.userId,
    };

UserRegisterResponse _$UserRegisterResponseFromJson(
        Map<String, dynamic> json) =>
    UserRegisterResponse(
      json['result'] == null
          ? null
          : UserRegisterDataResponse.fromJson(
              json['result'] as Map<String, dynamic>),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$UserRegisterResponseToJson(
        UserRegisterResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

UserRegisterDataResponse _$UserRegisterDataResponseFromJson(
        Map<String, dynamic> json) =>
    UserRegisterDataResponse(
      json['canLogin'] as bool?,
    );

Map<String, dynamic> _$UserRegisterDataResponseToJson(
        UserRegisterDataResponse instance) =>
    <String, dynamic>{
      'canLogin': instance.canLogin,
    };

GetAllCitiesResponse _$GetAllCitiesResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllCitiesResponse(
      (json['result'] as List<dynamic>?)
          ?.map((e) => CityResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$GetAllCitiesResponseToJson(
        GetAllCitiesResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

CityResponse _$CityResponseFromJson(Map<String, dynamic> json) => CityResponse(
      json['countryId'] as int?,
      json['countryName'] as String?,
      json['name'] as String?,
      json['id'] as int?,
    );

Map<String, dynamic> _$CityResponseToJson(CityResponse instance) =>
    <String, dynamic>{
      'countryId': instance.countryId,
      'countryName': instance.countryName,
      'name': instance.name,
      'id': instance.id,
    };

GetAllCountriesResponse _$GetAllCountriesResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllCountriesResponse(
      (json['result'] as List<dynamic>?)
          ?.map((e) => CountryResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$GetAllCountriesResponseToJson(
        GetAllCountriesResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

CountryResponse _$CountryResponseFromJson(Map<String, dynamic> json) =>
    CountryResponse(
      json['name'] as String?,
      (json['cities'] as List<dynamic>?)
          ?.map((e) => CityResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CountryResponseToJson(CountryResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
      'cities': instance.cities,
    };

GetAllCityPartnersResponse _$GetAllCityPartnersResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllCityPartnersResponse(
      (json['result'] as List<dynamic>?)
          ?.map((e) => CityPartnersResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$GetAllCityPartnersResponseToJson(
        GetAllCityPartnersResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

CityPartnersResponse _$CityPartnersResponseFromJson(
        Map<String, dynamic> json) =>
    CityPartnersResponse(
      json['id'] as int?,
      json['prtnerId'] as int?,
      json['cityId'] as int?,
      json['age'] as int?,
      json['gender'] as int?,
      json['date'] == null ? null : DateTime.parse(json['date'] as String),
      json['userName'] as String?,
      json['cityName'] as String?,
    );

Map<String, dynamic> _$CityPartnersResponseToJson(
        CityPartnersResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'prtnerId': instance.prtnerId,
      'cityId': instance.cityId,
      'age': instance.age,
      'gender': instance.gender,
      'date': instance.date?.toIso8601String(),
      'userName': instance.userName,
      'cityName': instance.cityName,
    };

DownloadImageResponse _$DownloadImageResponseFromJson(
        Map<String, dynamic> json) =>
    DownloadImageResponse(
      json['result'] as String?,
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$DownloadImageResponseToJson(
        DownloadImageResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

GetAllChatListResponse _$GetAllChatListResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllChatListResponse(
      (json['result'] as List<dynamic>?)
          ?.map((e) =>
              GetAllChatListDataResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    )
      ..targetUrl = json['targetUrl'] as String?
      ..success = json['success'] as bool?
      ..error = json['error'] == null
          ? null
          : ErrorResponse.fromJson(json['error'] as Map<String, dynamic>)
      ..unAuthorizedRequest = json['unAuthorizedRequest'] as bool?
      ..abp = json['__abp'] as bool?;

Map<String, dynamic> _$GetAllChatListResponseToJson(
        GetAllChatListResponse instance) =>
    <String, dynamic>{
      'targetUrl': instance.targetUrl,
      'success': instance.success,
      'error': instance.error,
      'unAuthorizedRequest': instance.unAuthorizedRequest,
      '__abp': instance.abp,
      'result': instance.result,
    };

GetAllChatListDataResponse _$GetAllChatListDataResponseFromJson(
        Map<String, dynamic> json) =>
    GetAllChatListDataResponse(
      json['id'] as String?,
      json['accountId'] as int?,
      json['fullName'] as String?,
      json['contactId'] as int?,
      json['lastMessage'] as String?,
      json['lastMessageTime'] == null
          ? null
          : DateTime.parse(json['lastMessageTime'] as String),
      json['dialogs'] == null
          ? null
          : DialogsResponse.fromJson(json['dialogs'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetAllChatListDataResponseToJson(
        GetAllChatListDataResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'accountId': instance.accountId,
      'fullName': instance.fullName,
      'contactId': instance.contactId,
      'lastMessage': instance.lastMessage,
      'lastMessageTime': instance.lastMessageTime?.toIso8601String(),
      'dialogs': instance.dialogs,
    };

DialogsResponse _$DialogsResponseFromJson(Map<String, dynamic> json) =>
    DialogsResponse(
      json['id'] as int?,
      json['who'] as int?,
      json['message'] as String?,
      json['chatid'] as int?,
      json['time'] == null ? null : DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$DialogsResponseToJson(DialogsResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'who': instance.who,
      'message': instance.message,
      'chatid': instance.chatid,
      'time': instance.time?.toIso8601String(),
    };
