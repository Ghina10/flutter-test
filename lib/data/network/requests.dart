class LoginRequest {
  String email;
  String password;
  bool rememberClient;

  LoginRequest(this.email, this.password, this.rememberClient);
}

class RegisterRequest {
  String name;
  int gender;
  int age;
  int cityId;
  int countryId;
  String phone;
  String email;
  String password;
  String avatar;

  RegisterRequest(this.name, this.gender, this.age, this.cityId, this.countryId,
      this.phone, this.email, this.password, this.avatar);
}

class GetAllCityPartnersRequest {
  int cityId;
  int CountryId;
  int? Gender;
  int? MinAge;
  int? MaxAge;
  DateTime? Date;
  String? SkipCount;
  String? MaxResultCount;

  GetAllCityPartnersRequest(this.cityId, this.CountryId, this.Gender,
      this.MinAge, this.MaxAge, this.Date, this.SkipCount, this.MaxResultCount);
}
