import 'package:dio/dio.dart';

import '../../app/app_prefs.dart';
import '../../app/constants.dart';
import '../response/responses.dart';
import 'app_api.dart';

class AppServiceClient implements AppService {
  AppServiceClient(this._dio);

  final Dio _dio;
  String? userToken;

  @override
  Future<UserLoginResponse> login(email, password, rememberClient) async {
    print("login :$email");
    final _headers = <String, dynamic>{'Accept': 'application/json'};
    final _data = {
      'userNameOrEmailAddress': email,
      'password': password,
      'rememberClient': rememberClient
    };
    print(_data);
    final _result = await _dio.post(AppUrls.loginUrl,
        data: _data, options: Options(headers: _headers));
    print("login data:${_result.data}");
    final value = UserLoginResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<UserRegisterResponse> register(name, gender, age, cityId, countryId,
      phone, email, password, avatar) async {
    final _headers = <String, dynamic>{'Accept': 'application/json'};
    final _data = {
      "name": name,
      "gender": gender,
      "age": age,
      "cityId": cityId,
      "countryId": countryId,
      "phoneNumber": phone,
      "emailAddress": email,
      "password": password,
      "avatar": avatar
    };
    // _data.removeWhere((k, v) => v == null);
    print(_data);
    final _result = await _dio.post(AppUrls.registerUrl,
        data: _data, options: Options(headers: _headers));
    final value = UserRegisterResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }

  @override
  Future<GetAllCitiesResponse> gatAllCities() async {
    final _headers = <String, dynamic>{'Accept': 'application/json'};
    final _result =
        await _dio.get(AppUrls.allCityUrl, options: Options(headers: _headers));
    final value = GetAllCitiesResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }

  @override
  Future<GetAllCountriesResponse> gatAllCountries() async {
    final _headers = <String, dynamic>{'Accept': 'application/json'};
    final _result = await _dio.get(AppUrls.allCountryUrl,
        options: Options(headers: _headers));
    final value = GetAllCountriesResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }

  @override
  Future<GetAllCityPartnersResponse> getAllCityPartners(
      int cityId,
      int CountryId,
      int? Gender,
      int? MinAge,
      int? MaxAge,
      DateTime? Date,
      String? SkipCount,
      String? MaxResultCount) async {
    await AppPreferences().getAccessToken().then((value) => userToken = value);
    // print("ghinaaa $userToken");
    final _headers = <String, dynamic>{
      'Accept': 'application/json',
      'Authorization': 'Bearer $userToken'
    };

    final _result = await _dio.get(
        AppUrls.getAllCityPartnersUrl +
            "?CityId=$cityId" +
            "&CountryId=$CountryId" +
            "${Gender != null ? "&Gender=$Gender" : ""}" +
            "${MinAge != null ? "&MinAge=$MinAge" : ""}" +
            "${MaxAge != null ? "&MaxAge=$MaxAge" : ""}" +
            "${Date != null ? "&Date=$Date" : ""}",
        options: Options(headers: _headers));
    final value = GetAllCityPartnersResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }

  @override
  Future<DownloadImageResponse> downloadImage(int idImage) async {
    final _headers = <String, dynamic>{'Accept': 'application/json'};
    final _result = await _dio.get(AppUrls.downloadImageUrl + "$idImage",
        options: Options(headers: _headers));
    final value = DownloadImageResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }

  @override
  Future<GetAllChatListResponse> getAllChatList() async {
    await AppPreferences().getAccessToken().then((value) => userToken = value);
    final _headers = <String, dynamic>{
      'Accept': 'application/json',
      'Authorization': 'Bearer $userToken'
    };
    final _result = await _dio.get(AppUrls.getAllChatListUrl,
        options: Options(headers: _headers));
    final value = GetAllChatListResponse.fromJson(_result.data!);
    //print(_result.data!['result']);
    return value;
  }
}
