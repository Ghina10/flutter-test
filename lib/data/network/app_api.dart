import '../response/responses.dart';

abstract class AppService {
  //factory AppServiceClient(Dio dio, {String baseUrl}) = _AppServiceClient;

  Future<UserLoginResponse> login(
      String email, String password, bool rememberClient);

  Future<UserRegisterResponse> register(
      String name,
      int gender,
      int age,
      int cityId,
      int countryId,
      String phone,
      String email,
      String password,
      String avatar);

  Future<GetAllCitiesResponse> gatAllCities();

  Future<GetAllCountriesResponse> gatAllCountries();

  Future<GetAllCityPartnersResponse> getAllCityPartners(
      int cityId,
      int CountryId ,
      int? Gender,
      int? MinAge,
      int? MaxAge,
      DateTime? Date,
      String? SkipCount,
      String? MaxResultCount);

  Future<DownloadImageResponse> downloadImage(int idImage);

  Future<GetAllChatListResponse> getAllChatList();
}
