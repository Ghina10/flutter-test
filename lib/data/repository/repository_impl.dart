import 'package:dartz/dartz.dart';
import 'package:job_application_test/data/mapper/mapper.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../app/app_prefs.dart';
import '../../domain/model/user.dart';
import '../../domain/repository/repository.dart';
import '../data_source/local_data_source.dart';
import '../data_source/remote_data_source.dart';
import '../network/error_handler.dart';
import '../network/failure.dart';
import '../network/network_info.dart';
import '../network/requests.dart';
import '../response/responses.dart';

class RepositoryImpl implements Repository {
  final RemoteDataSource _remoteDataSource;
  final LocalDataSource _localDataSource;
  final NetworkInfo _networkInfo;

  RepositoryImpl(
      this._remoteDataSource, this._networkInfo, this._localDataSource);

  @override
  Future<Either<Failure, LoginInfo>> login(LoginRequest loginRequest) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.login(loginRequest);
        if (response.result != null) {
          // print(response.result);
          AppPreferences().setAccessToken(response.result!.accessToken!);
          AppPreferences()
              .setEncryptedAccessToken(response.result!.encryptedAccessToken!);
          AppPreferences()
              .setExpireInSeconds(response.result!.expireInSeconds!);
          AppPreferences().setUserId(response.result!.userId!);
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> register(
      RegisterRequest registerRequest) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.register(registerRequest);
        print(response.result!.canLogin);
        if (response.result != null) {
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        print(error);
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<City>>> getAllCity() async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.gatAllCities();
        if (response.result != null) {
          // print("sanaaa");
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<Country>>> getAllCountry() async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.gatAllCountries();
        if (response.result != null) {
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<GetAllCityPartners>>> getAllCityPartners(
      GetAllCityPartnersRequest getAllCityPartnersRequest) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource
            .getAllCityPartners(getAllCityPartnersRequest);
        if (response.result != null) {
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        print(error);
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, String>> downloadImage(id) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.downloadImage(id);
        if (response.result != null) {
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<GetAllChatList>>> getAllChatList() async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await _remoteDataSource.getAllChatList();
        if (response.result != null) {
          return Right(response.toDomain());
        } else {
          return Left(Failure(ApiInternalStatus.FAILURE,
              response.error!.message ?? ResponseMessage.DEFAULT));
        }
      } catch (error) {
        return Left(ErrorHandler.handle(error).failure);
      }
    } else {
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
