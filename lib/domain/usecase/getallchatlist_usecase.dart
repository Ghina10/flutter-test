import 'package:dartz/dartz.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../data/network/failure.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class GetAllChatListUseCase implements BaseUseCase<String?, List<GetAllChatList>> {
  final Repository _repository;

  GetAllChatListUseCase(this._repository);

  @override
  Future<Either<Failure, List<GetAllChatList>>> execute(String? nothing) async {
    return await _repository.getAllChatList();
  }
}
