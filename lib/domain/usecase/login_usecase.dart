import 'package:dartz/dartz.dart';

import '../../data/network/failure.dart';
import '../../data/network/requests.dart';
import '../model/user.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class LoginUseCase implements BaseUseCase<LoginUseCaseInput, LoginInfo> {
  final Repository _repository;

  LoginUseCase(this._repository);

  @override
  Future<Either<Failure, LoginInfo>> execute(LoginUseCaseInput? input) async {
    //print("Loginusecase:${input.email} ${input.password}");
    return await _repository.login(LoginRequest(input!.email, input.password,input.rememberClient));
  }
}

class LoginUseCaseInput {
  String email;
  String password;
  bool rememberClient;

  LoginUseCaseInput(this.email, this.password,this.rememberClient);
}
