import 'package:dartz/dartz.dart';

import '../../data/network/failure.dart';
import '../../data/network/requests.dart';
import '../../data/response/responses.dart';
import '../model/models.dart';
import '../model/user.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class GetAllCityPartnersUseCase
    implements BaseUseCase<GetAllCityPartnersUseCaseInput, List<GetAllCityPartners>> {
  final Repository _repository;

  GetAllCityPartnersUseCase(this._repository);

  @override
  Future<Either<Failure, List<GetAllCityPartners>>> execute(
      GetAllCityPartnersUseCaseInput? input) async {
    //print("Loginusecase:${input.email} ${input.GetAllCityPartnersUseCaseInput}");
    return await _repository.getAllCityPartners(GetAllCityPartnersRequest(
        input!.cityId,
        input.CountryId,
        input.Gender,
        input.MinAge,
        input.MaxAge,
        input.Date,
        input.SkipCount,
        input.MaxResultCount));
  }
}

class GetAllCityPartnersUseCaseInput {
  int cityId;
  int CountryId;
  int? Gender;
  int? MinAge;
  int? MaxAge;
  DateTime? Date;
  String? SkipCount;
  String? MaxResultCount;

  GetAllCityPartnersUseCaseInput(this.cityId, this.CountryId, this.Gender,
      this.MinAge, this.MaxAge, this.Date, this.SkipCount, this.MaxResultCount);
}
