import 'package:dartz/dartz.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../data/network/failure.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class GetAllCityUseCase implements BaseUseCase<String?, List<City>> {
  final Repository _repository;

  GetAllCityUseCase(this._repository);

  @override
  Future<Either<Failure, List<City>>> execute(String? nothing) async {
    return await _repository.getAllCity();
  }
}
