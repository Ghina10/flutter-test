import 'package:dartz/dartz.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../data/network/failure.dart';
import '../../data/network/requests.dart';
import '../model/user.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class GetAllCountryUseCase implements BaseUseCase<String?, List<Country>> {
  final Repository _repository;

  GetAllCountryUseCase(this._repository);

  @override
  Future<Either<Failure, List<Country>>> execute(String? nothing) async {
    return await _repository.getAllCountry();
  }
}
