import 'package:dartz/dartz.dart';

import '../../data/network/failure.dart';
import '../../data/network/requests.dart';
import '../model/user.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class RegisterUseCase implements BaseUseCase<RegisterUseCaseInput, bool> {
  final Repository _repository;

  RegisterUseCase(this._repository);

  @override
  Future<Either<Failure, bool>> execute(RegisterUseCaseInput? input) async {
    return await _repository.register(RegisterRequest(
        input!.name, input.gender, input.age, input.cityId,
        input.countryId, input.phone, input.email, input.password,input.avatar));
  }
}

class RegisterUseCaseInput {
  String name;
  int gender;
  int age;
  int cityId;
  int countryId;
  String phone;
  String email;
  String password;
  String avatar;

  RegisterUseCaseInput(this.name, this.gender, this.age, this.cityId,
      this.countryId, this.phone, this.email, this.password, this.avatar);
}
