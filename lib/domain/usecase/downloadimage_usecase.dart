import 'package:dartz/dartz.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../data/network/failure.dart';
import '../repository/repository.dart';
import 'base_usecase.dart';

class DownloadImageUseCase implements BaseUseCase<int?, String> {
  final Repository _repository;

  DownloadImageUseCase(this._repository);

  @override
  Future<Either<Failure, String>> execute(int? id) async {
    return await _repository.downloadImage(id!);
  }
}


class DownloadImageUseCaseInput {
  int idImage;

  DownloadImageUseCaseInput(this.idImage);
}

