import 'package:dartz/dartz.dart';
import 'package:job_application_test/domain/model/models.dart';

import '../../data/network/failure.dart';
import '../../data/network/requests.dart';
import '../../data/response/responses.dart';
import '../model/user.dart';

abstract class Repository {
  Future<Either<Failure, LoginInfo>> login(LoginRequest loginRequest);

  Future<Either<Failure, bool>> register(RegisterRequest registerRequest);

  Future<Either<Failure, List<City>>> getAllCity();

  Future<Either<Failure, List<Country>>> getAllCountry();

  Future<Either<Failure, List<GetAllCityPartners>>> getAllCityPartners(
      GetAllCityPartnersRequest getAllCityPartnersRequest);

  Future<Either<Failure, String>> downloadImage(int id);

  Future<Either<Failure, List<GetAllChatList>>> getAllChatList();
}
