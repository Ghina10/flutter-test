// login models
class User {
  String id;
  String userName;
  String name;
  String surname;
  String emailAddress;
  String isActive;
  String fullName;
  String lastLoginTime;
  String creationTime;
  String roleNames;

  User(this.id, this.userName,this.name, this.surname, this.emailAddress, this.isActive,
      this.fullName, this.lastLoginTime, this.creationTime, this.roleNames);
}

class Authentication {
  User? user;

  Authentication(this.user);
}

class LoginInfo{
  String? accessToken;
  String? encryptedAccessToken;
  int? expireInSeconds;
  int? userId;

  LoginInfo(this.accessToken, this.encryptedAccessToken, this.expireInSeconds,
      this.userId);
}