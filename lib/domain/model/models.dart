class City {
  int countryId;
  String countryName;
  String name;
  int id;

  City(this.countryId, this.countryName, this.name, this.id);
}

class Country{
  String name;
  List<City> city;

  Country(this.name, this.city);
}


class GetAllCityPartners{
  int? id;
  int? prtnerId;
  int? cityId;
  int? age;
  int? gender;
  DateTime? date;
  String? userName;
  String? cityName;

  GetAllCityPartners(this.id, this.prtnerId, this.cityId, this.age, this.gender,
      this.date, this.userName, this.cityName);
}


class GetAllChatList{
  String? id;
  int? accountId;
  String? fullName;
  int? contactId;
  String? lastMessage;
  DateTime? lastMessageTime;
  Dialog? dialogs;

  GetAllChatList(this.id, this.accountId, this.fullName, this.contactId,
      this.lastMessage, this.lastMessageTime, this.dialogs);
}


class Dialog{
  int? id;
  int? who;
  String? message;
  int? chatid;
  DateTime? time;

  Dialog(this.id, this.who, this.message, this.chatid, this.time);
}
