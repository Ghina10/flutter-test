import 'package:get/get.dart';

import '../../domain/model/models.dart';
import '../widget/get_imag_controller/get_image_controller.dart';

class SuggestionController extends GetxController {
  final imageController = Get.find<ImageController>();

  List<GetAllCityPartners> _partners = [];

  List<GetAllCityPartners> get partners => _partners;

  set partners(List<GetAllCityPartners> value) {
    _partners = value;
  }

  @override
  void onInit() {
    partners = Get.arguments[0];
    //imageController.getImage(partners[0].id!);
    super.onInit();
  }

  final RxString _name = "".obs;
  String get name => _name.value;
  set name(String value){
    _name.value = value;
  }

  final RxInt _age = (1).obs;
  int get age => _age.value;
  set age(int value){
    _age.value = value;
  }

  final RxString _cityName = "".obs;
  String get cityName => _cityName.value;
  set cityName(String value){
    _cityName.value = value;
  }

}