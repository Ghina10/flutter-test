import 'package:get/get.dart';
import 'package:job_application_test/view/suggestionpartners/suggestion_controller.dart';

class SuggestionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SuggestionController());
  }
}
