import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/resources/strings_manager.dart';
import 'package:job_application_test/view/suggestionpartners/suggestion_controller.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/routes_manager.dart';
import '../resources/value_manager.dart';
import '../responsive/media_query.dart';

class SuggestionView extends GetView<SuggestionController> {
  const SuggestionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.suggestion),
      ),
      body: //5 < 0
          controller.partners.isEmpty
              ? buildEmptyScreen()
              : ListView.builder(
                  //itemExtent: 100,
                  itemCount: //5,
                      controller.partners.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        const SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 18.0),
                          child: Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: const <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.black54,
                                        blurRadius: 5.0,
                                        offset: Offset(0.0, 0.5))
                                  ],
                                ),
                                height: 150,
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, top: 8),
                                        child: Container(
                                          height: AppSize.s120,
                                          width: AppSize.s120,
                                          margin: const EdgeInsets.symmetric(
                                              vertical: AppMargin.m2,
                                              horizontal: AppMargin.m2),
                                          //padding: const EdgeInsets.all(AppPadding.p5),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                                AppSize.s12),
                                            border: Border.all(
                                                width: 2,
                                                color: ColorManager.black),
                                          ),
                                          child: Image.asset(controller
                                              .imageController
                                              .getImage(controller
                                                  .partners[index].id)),
                                          // Image.asset(ImageAssets.image),
                                        ),
                                      ),
                                      Container(
                                        height: 120,
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(controller
                                                .partners[index].userName!),
                                            Text(
                                                "${AppStrings.age} : ${controller.partners[index].age}"),
                                            Text(
                                                "${AppStrings.cityName}: ${controller.partners[index].cityName}"),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      elevation: 3,
                                      fixedSize: Size(getWidth(context) * 0.85,
                                          AppSize.s16),
                                    ),
                                    onPressed: () async {
                                      Get.toNamed(Routes.suggestionRoute);
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.message_rounded,
                                          color: Colors.white,
                                        ),
                                        const SizedBox(width: 10),
                                        Text(
                                          AppStrings.sendMessage,
                                          style: Theme.of(context)
                                              .textTheme
                                              .displayLarge,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }),
    );
  }

  Widget buildEmptyScreen() {
    return const Center(
      child: Text(AppStrings.sorry),
    );
  }
}
