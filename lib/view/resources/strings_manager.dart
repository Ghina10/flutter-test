class AppStrings {
  static const String login = "Login";
  static const String name = "Name";
  static const String age = "Age";
  static const String gender = "Gender";
  static const String country = "Country";
  static const String city = "City";
  static const String phone = "Phone number";
  static const String email = "Email";
  static const String password = "Password";
  static const String reenterPassword = "Reenter Password";
  static const String forgetPassword = "Forget Password?";
  static const String creatAccount = "Don't have an account? ";
  static const String notCreatAccount = "Already have an account? ";
  static const String signUp = "Sign Up";
  static const String terms = "Accept the Terms & services";
  static const String remember = "Remember me";
  static const String home = "Home";
  static const String choose = "Choose upload";
  static const String gallery = "Gallery";
  static const String camera = "Camera";
  static const String findpartner = "Find a partner";
  static const String partnerAge = "Partner's age:";
  static const String between = "Between";
  static const String and = "And";
  static const String date = "Date: / /";
  static const String search = "Search";
  static const String message = "Message";
  static const String suggestion = "Suggestion partners";
  static const String sendMessage = "Send a message";
  static const String done = "Done";
  static const String Sign = "Sign in!";
  static const String pleaseSign = "Please sign in \n to continue to the app";
  static const String pleaseLogin = "please login to continue";
  static const String username = 'User Name';
  static const String messageContact = "Message contact";
  static const String ooops = "ooops! \n No message yet";
  static const String sorry = "Sorry there is no partner";
  static const String cityName = "City name";



  static const usernamehint = "username_hint";
  static const passwordhint = "password_hint";
  static const usernameError = "username_error";
  static const passwordError = "password_error";
  static const loginbutton = "login_button";
  static const forgetPasswordtext = "forgot_password_text";
  static const registerText = "register_text";
  static const loading = "loading";
  static const retryAgain = "retry_again";
  static const ok = "ok";
  static const String emailHint = 'email_hint';
  static const String invalidEmail = "email_error";
  static const String invalidName = "invalid Name";
  static const String invalidAge = "invalid Age";
  static const String invalidPhone = "invalid Phone";

  //static const String resetPassword = "reset_password";
  static const String success = "success";
  static const String userNameInvalid = "username_hint_message";
  static const String mobileNumberInvalid = "mobile_number_hint_message";
  static const String passwordInvalid = "password_hint_message";
  static const String confirPasswordInvalid = "confirm password_hint_message";
  static const String alreadyHaveAccount = "already_have_account";
  static const register = "register";
  static const mobileNumber = "mobile_number_hint";
  static const profilePicture = "upload_profile_picture";
  static const photoGallery = "photo_from_galley";
  static const photoCamera = "photo_from_camera";


  // error handler
  static const String badRequestError = "bad_request_error";
  static const String noContent = "no_content";
  static const String forbiddenError = "forbidden_error";
  static const String unauthorizedError = "unauthorized_error";
  static const String notFoundError = "not_found_error";
  static const String conflictError = "conflict_error";
  static const String internalServerError = "internal_server_error";
  static const String unknownError = "unknown_error";
  static const String timeoutError = "timeout_error";
  static const String defaultError = "default_error";
  static const String cacheError = "cache_error";
  static const String noInternetError = "no_internet_error";



}
