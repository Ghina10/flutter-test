
import 'package:get/get.dart';
import 'package:job_application_test/view/home/home_binding.dart';
import 'package:job_application_test/view/home/home_view.dart';
import 'package:job_application_test/view/login/login_binding.dart';
import 'package:job_application_test/view/login/login_view.dart';
import 'package:job_application_test/view/message/message_binding.dart';
import 'package:job_application_test/view/message/message_view.dart';
import 'package:job_application_test/view/register/add/afterRegister_View.dart';
import 'package:job_application_test/view/register/add/beforRegister.dart';
import 'package:job_application_test/view/register/registerBinding.dart';
import 'package:job_application_test/view/register/register_view.dart';
import 'package:job_application_test/view/search/search_binding.dart';
import 'package:job_application_test/view/search/search_view.dart';
import 'package:job_application_test/view/splash/splash_binding.dart';
import 'package:job_application_test/view/suggestionpartners/suggestion_binding.dart';
import 'package:job_application_test/view/suggestionpartners/suggestion_view.dart';

import '../bottom_navigation/pandabar.dart';
import '../bottom_navigation/pandabar_binding.dart';
import '../splash/splash_view.dart';


class Routes {
  static const String splashRoute = "/";
  static const String loginRoute = "/login";
  static const String registerRoute = "/register";
  static const String afterRegisterRoute = "/afterRegister";
  static const String beforeRegisterRoute = "/beforeRegister";
  static const String homeRoute = '/HomeScreen';
  static const String messageRoute = '/message';
  static const String searchRoute = '/search';
  static const String suggestionRoute = '/suggestion';
}

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: Routes.splashRoute,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.registerRoute,
      page: () => RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: Routes.loginRoute,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.afterRegisterRoute,
      page: () => const AfterRegisterView(),
    ),
    GetPage(
      name: Routes.beforeRegisterRoute,
      page: () => const BeforeRegisterView(),
    ),
    GetPage(
      name: Routes.homeRoute,
      page: () => PandaNavigationBar(),
      binding: PandaBarBinding(),
    ),
    GetPage(
      name: Routes.messageRoute,
      page: () => const MessageView(),
      binding: MessageBinding(),
    ),
    GetPage(
      name: Routes.searchRoute,
      page: () => SearchView(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: Routes.suggestionRoute,
      page: () => const SuggestionView(),
      binding: SuggestionBinding(),
    ),
  ];
}
