class AppMargin {
  static const double m2 = 2.0;
   static const double m8 = 8.0;
  // static const double m12 = 12.0;
  // static const double m14 = 14.0;
  // static const double m16 = 16.0;
  // static const double m18 = 18.0;
  // static const double m20 = 20.0;
}

class AppPadding {
  static const double p5 = 5.0;
  static const double p8 = 8.0;
  static const double p10 = 10.0;
  static const double p12 = 12.0;
  static const double p15 = 15.0;

  // static const double p16 = 16.0;
  static const double p18 = 18.0;

  // static const double p20 = 20.0;
  static const double p24 = 24.0;
  static const double p28 = 28.0;
  static const double p35 = 35.0;
// static const double p60 = 60.0;
// static const double p100 = 100.0;
}

class AppSize {
  // static const double s0 = 0;
  static const double s1_5 = 1.5;
  static const double s4 = 4.0;
  static const double s8 = 8.0;
  static const double s12 = 12.0;

  // static const double s14 = 14.0;
  static const double s16 = 16.0;

  static const double s18 = 18.0;
  static const double s20 = 20.0;
  static const double s28 = 28.0;
  static const double s35 = 35.0;
  static const double s40 = 40.0;
  static const double s50 = 50.0;

  // static const double s60 = 60.0;
  static const double s70 = 70.0;
  static const double s100 = 100.0;
  static const double s110 = 110.0;
  static const double s120 = 120.0;
  static const double s150 = 150.0;
  static const double s175 = 175.0;
  static const double s200 = 200.0;
  static const double s230 = 230.0;
  static const double s280 = 280.0;
  static const double s300 = 300.0;
}

class DurationConstant {
  // static const int d300 = 300;

}
