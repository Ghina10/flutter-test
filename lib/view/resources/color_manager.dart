import 'package:flutter/material.dart';

class ColorManager {

  static Color orange = const Color(0xFFFF6F00);
  static Color grey = const Color(0xFF9E9E9E);

  static Color blackwhite = const Color(0xFFEBF2FA);
  static Color white = const Color(0xFFFFFFFF);
  static Color black = const Color(0xFF000000);
  static Color error = const Color(0xFF831A26);
}
