const String IMAGE_PATH = "assets/images";

class ImageAssets {
  static const String splash = "$IMAGE_PATH/bg.png";
 // static const String welcome = "$IMAGE_PATH/welcome.png";
  static const String image = "$IMAGE_PATH/icon/image.png";
  static const String welcome = "$IMAGE_PATH/welcome.svg";

}