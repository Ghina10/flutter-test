import 'package:flutter/material.dart';

import 'font_manager.dart';
import 'value_manager.dart';

MaterialColor primarySwatch = const MaterialColor(0xFF71A5D7, {
  50: Color(0xFFFF6F00),
  100: Color(0xFFFF6F00),
  200: Color(0xFFFF6F00),
  300: Color(0xFFFF6F00),
  400: Color(0xFFFF6F00),
  500: Color(0xFFFF6F00),
  600: Color(0xFFFF6F00),
  700: Color(0xFFFF6F00),
  800: Color(0xFFFF6F00),
  900: Color(0xFFFF6F00)
});

Color lightGray3 = const Color(0xffBFBFBF);
// Color lightGray4 = const Color(0xffEBE9E9);
// Color black = const Color(0xff131313);

Color orange = const Color(0xFFFF6F00);
Color grey = const Color(0xFF9E9E9E);

Color blackwhite = const Color(0xFFEBF2FA);
Color white = const Color(0xFFFFFFFF);
Color black = const Color(0xFF000000);
Color error = const Color(0xFF831A26);

ThemeData getLightTheme() {
  return ThemeData(
    //primaryIconTheme: IconThemeData(color: white),
    //iconTheme: IconThemeData(color: white),
    brightness: Brightness.light,
    primarySwatch: primarySwatch,
    primaryColor: white,
    primaryColorLight: orange,
    scaffoldBackgroundColor: white,
    // to change material color and containers colors
    backgroundColor: white,
    //secondaryHeaderColor: black,
    shadowColor: grey,

    unselectedWidgetColor: lightGray3,
    selectedRowColor: orange,
    errorColor: error,
    progressIndicatorTheme: ProgressIndicatorThemeData(color: orange),

    // main colors of the app
    appBarTheme: AppBarTheme(
        toolbarHeight: 60,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(8),
          ),
        ),
        centerTitle: true,
        color: orange,
        elevation: 0,
        shadowColor: grey,
        titleTextStyle: TextStyle(
            fontSize: FontSize.s20, color: white, fontWeight: FontWeight.bold)),
    // card view theme
    //cardColor: Colors.transparent,
    cardTheme: const CardTheme(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(AppSize.s20),
          ),
          side: BorderSide(color: Colors.white)),
      color: Colors.transparent,
      // shadowColor:  grey,
     // elevation: AppSize.s4,
    ),
    textTheme: TextTheme(
      displayLarge: TextStyle(
          color: white,
          fontSize: FontSize.s17,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.bold),
      displayMedium: TextStyle(
          color: orange,
          fontSize: FontSize.s17,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.bold),
      displaySmall: TextStyle(
          color: orange,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      headlineMedium: TextStyle(
          color: black,
          fontSize: FontSize.s24,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      headlineSmall: TextStyle(
          color: black,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      titleLarge: TextStyle(
          color: black,
          fontSize: FontSize.s27,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.semiBold),
      titleMedium: TextStyle(
          color: black,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      titleSmall: TextStyle(
          color: black,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      bodyLarge: TextStyle(
          color: black,
          fontSize: FontSize.s40,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.semiBold),
      bodyMedium: TextStyle(
          color: black,
          fontSize: FontSize.s17,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.semiBold),
      bodySmall: TextStyle(
          color: orange,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      labelLarge: TextStyle(
          color: black,
          fontSize: FontSize.s16,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.semiBold),
      labelSmall: TextStyle(
          color: black,
          fontSize: FontSize.s12,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.light),
    ),

    // elevated button theme
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        //textStyle: getRegularStyle(color:  white),
        primary: orange,
        elevation: 10.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppSize.s12)),
        textStyle: TextStyle(color: white, fontSize: 17),
      ),
    ),

    inputDecorationTheme: InputDecorationTheme(
      fillColor: white,
      iconColor: black,
      contentPadding: const EdgeInsets.all(AppPadding.p12),
      // hint style
      hintStyle: TextStyle(
          color: grey,
          fontSize: FontSize.s16,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.semiBold),
      // label style
      labelStyle: TextStyle(
          color: grey,
          fontSize: FontSize.s14,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),
      // error style
      errorStyle: TextStyle(
          color: error,
          fontSize: FontSize.s12,
          fontFamily: FontConstants.fontFamily,
          fontWeight: FontWeightManager.regular),

      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(AppSize.s8),
        borderSide: BorderSide(
          color: black,
          width: AppSize.s1_5,
        ),
      ),

      // enabled border
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: black,
          width: AppSize.s1_5,
        ),
        borderRadius: BorderRadius.circular(AppSize.s8),
      ),

      // focused border
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: black, width: AppSize.s1_5),
        borderRadius: BorderRadius.circular(AppSize.s8),
      ),

      // error border
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: error, width: AppSize.s1_5),
        borderRadius: BorderRadius.circular(AppSize.s8),
      ),

      // focused error border
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: black, width: AppSize.s1_5),
        borderRadius: BorderRadius.circular(AppSize.s8),
      ),
    ),
  );
}

