import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/register/register_controller.dart';
import 'package:job_application_test/view/responsive/media_query.dart';
import 'package:job_application_test/view/widget/cities_drop_down.dart';
import 'package:job_application_test/view/widget/countries_drop_down.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/routes_manager.dart';
import '../resources/strings_manager.dart';
import '../resources/value_manager.dart';

class RegisterView extends GetView<RegisterController> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            buildBackground(),
            buildCard(context),
          ],
        ),
      ),
    );
  }

  Widget buildCard(context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: getHeight(context) * 0.05),
          buildUploadImage(context),
          buildText(context),
        ],
      ),
    );
  }

  Widget buildBackground() {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(ImageAssets.splash),
          fit: BoxFit.none,
        ),
      ),
    );
  }

  Widget buildUploadImage(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            Text(
              AppStrings.signUp,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            Obx(
              () => Container(
                height: AppSize.s110,
                width: AppSize.s110,
                margin: const EdgeInsets.symmetric(
                    vertical: AppMargin.m8, horizontal: AppMargin.m8),
                padding: const EdgeInsets.all(AppPadding.p15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.s18),
                  border: Border.all(width: 2, color: ColorManager.black),
                ),
                child: controller.image.path == ""
                    ? Image.asset(ImageAssets.image)
                    : Image.file(controller.image),
              ),
            ),
          ],
        ),
        GestureDetector(
          child: const Icon(
            Icons.border_color_outlined,
            size: 30,
          ),
          onTap: () => controller.openImageDialog(),
        ),
      ],
    );
  }

  Widget buildText(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppPadding.p8),
      child: Container(
        color: Colors.transparent,
        child: Form(
          key: _formkey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppPadding.p15),
            child: Column(
              children: [
                const SizedBox(height: AppSize.s28),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      //   controller: _emailController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                          hintText: AppStrings.name,
                          errorText: controller.isNameValid
                              ? null
                              : AppStrings.invalidName),
                      onChanged: (name) => controller.setName(name),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      // controller: _userNameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: AppStrings.age,
                          errorText: controller.isAgeValid
                              ? null
                              : AppStrings.invalidAge),
                      onChanged: (age) => controller.setAge(int.parse(age)),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: buildGenderDropDown(),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                    child: buildCountriesDropDown()),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: buildCitiesDropDown(),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      // controller: _userNameController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          hintText: AppStrings.phone,
                          errorText: controller.isPhoneValid
                              ? null
                              : AppStrings.invalidPhone),
                      onChanged: (phone) => controller.setPhone(phone),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      //   controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          hintText: AppStrings.email,
                          suffixIcon: const Icon(
                            Icons.email,
                            size: 22,
                          ),
                          errorText: controller.isEmailValid
                              ? null
                              : AppStrings.invalidEmail),
                      onChanged: (email) => controller.setEmail(email),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      //  controller: _PasswordController,
                      obscureText: !controller.isShowPassword,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: AppStrings.password,
                          errorText: controller.isPasswordValid
                              ? null
                              : AppStrings.passwordInvalid,
                          suffixIcon: GestureDetector(
                            onTap: () {
                              controller.setShowPassword();
                            },
                            child: Icon(
                              controller.isShowPassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: ColorManager.grey,
                              size: 22,
                            ),
                          )),
                      onChanged: (password) => controller.setPassword(password),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Obx(
                    () => TextFormField(
                      //   controller: _ConfirmPasswordController,
                      obscureText: !controller.showConfirmPassword,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: AppStrings.reenterPassword,
                          errorText: controller.isReenterPasswordValid
                              ? null
                              : AppStrings.confirPasswordInvalid,
                          suffixIcon: GestureDetector(
                            onTap: () {
                              controller.setShowConfirmPassword();
                            },
                            child: Icon(
                              controller.showConfirmPassword
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: ColorManager.grey,
                              size: 22,
                            ),
                          )),
                      onChanged: (confirmPassword) =>
                          controller.setReenterPassword(confirmPassword),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppPadding.p12),
                  child: Row(
                    children: [
                      Obx(
                        () => Checkbox(
                          value: controller.isChecked,
                          onChanged: (bool? value) {
                            controller.isChecked = value!;
                          },
                        ),
                      ),
                      Obx(
                        () => controller.isChecked
                            ? Text(AppStrings.terms,
                                style: Theme.of(context).textTheme.bodySmall)
                            : Text(AppStrings.terms,
                                style: Theme.of(context).textTheme.titleSmall),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: AppSize.s18),
                Obx(
                  () => controller.startRegister
                      ? const Center(child: CircularProgressIndicator())
                      : ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            fixedSize: Size(
                                getWidth(context) - AppSize.s100, AppSize.s50),
                          ),
                          onPressed: () async {
                            await controller.register();
                            controller.isUserRegisterSuccessfully
                                ? Get.offNamed(Routes.afterRegisterRoute)
                                : () {};
                          },
                          child: Text(
                            AppStrings.signUp,
                            style: Theme.of(context).textTheme.displayLarge,
                          ),
                        ),
                ),
                const SizedBox(height: AppSize.s18),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: AppPadding.p8, right: AppPadding.p18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          Get.offNamed(Routes.loginRoute);
                        },
                        child: Row(
                          children: [
                            Text(
                              AppStrings.notCreatAccount,
                              style: Theme.of(context).textTheme.headlineSmall,
                            ),
                            Text(
                              AppStrings.login,
                              style: Theme.of(context).textTheme.displaySmall,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildCountriesDropDown() {
    return const CountriesDropDown();
  }

  Widget buildCitiesDropDown() {
    return const CitiesDropDown();
  }

  Widget buildGenderDropDown() {
    return InputDecorator(
      decoration: const InputDecoration(
        hintText: AppStrings.gender,
      ),
      child: Obx(() {
        return DropdownButtonHideUnderline(
          child: DropdownButton2<String>(
              items: controller.genderItems
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              isExpanded: true,
              isDense: true,
              onChanged: (String? newSelectedGender) {
                controller.selectGender = newSelectedGender!;
              },
              value: controller.selectGender),
        );
      }),
    );
  }
}
