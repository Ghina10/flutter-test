import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/responsive/media_query.dart';

import '../../resources/assets_manager.dart';
import '../../resources/color_manager.dart';
import '../../resources/routes_manager.dart';
import '../../resources/strings_manager.dart';
import '../../resources/value_manager.dart';

class BeforeRegisterView extends StatelessWidget {
  const BeforeRegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            buildBackground(),
            buildCard(context),
          ],
        ),
      ),
    );
  }

  Widget buildBackground() {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(ImageAssets.splash),
          fit: BoxFit.none,
        ),
      ),
    );
  }

  Widget buildCard(context) {
    return Center(
      child: SizedBox(
        height: getHeight(context) * 0.25,
        width: getWidth(context),
        child: Column(
          children: [
            Text(
              AppStrings.pleaseLogin,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                fixedSize: Size(getWidth(context) - AppSize.s100, AppSize.s50),
              ),
              onPressed: () async {
                Get.offAllNamed(Routes.loginRoute);
              },
              child: Text(
                AppStrings.login,
                style: Theme.of(context).textTheme.displayLarge,
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Get.theme.primaryColor,
                elevation: 15.0,
                side: BorderSide(
                    color: Get.theme.primaryColorLight, width: AppSize.s1_5),
                fixedSize: Size(getWidth(context) - AppSize.s100, AppSize.s50),
                textStyle:
                TextStyle(color: Get.theme.primaryColorLight, fontSize: 17),
              ),
              onPressed: () {
                Get.offNamed(Routes.registerRoute);
              },
              child: Text(
                AppStrings.signUp,
                style: Theme.of(context).textTheme.displayMedium,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
