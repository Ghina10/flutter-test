import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/responsive/media_query.dart';

import '../../resources/assets_manager.dart';
import '../../resources/color_manager.dart';
import '../../resources/routes_manager.dart';
import '../../resources/strings_manager.dart';
import '../../resources/value_manager.dart';

class AfterRegisterView extends StatelessWidget {
  const AfterRegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            buildCard(context),
          ],
        ),
      ),
    );
  }

  Widget buildCard(context) {
    return Center(
      child: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppStrings.Sign,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 20),
            Text(
              AppStrings.pleaseSign,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                fixedSize: Size(getWidth(context) - AppSize.s100, AppSize.s50),
              ),
              onPressed: () async {
                Get.offAllNamed(Routes.loginRoute);
              },
              child: Text(
                AppStrings.done,
                style: Theme.of(context).textTheme.displayLarge,
              ),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
