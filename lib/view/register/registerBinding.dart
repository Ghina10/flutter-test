import 'package:get/get.dart';
import 'package:job_application_test/view/register/register_controller.dart';

import '../../app/di.dart';
import '../widget/dropdown_controllers/cities_controller.dart';
import '../widget/dropdown_controllers/countries_controller.dart';
class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RegisterController(NoteAppDependency().repo));
    Get.lazyPut(() => CountriesController(NoteAppDependency().repo));
    Get.lazyPut(() => CitiesController(NoteAppDependency().repo));
  }
}
