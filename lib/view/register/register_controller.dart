import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../app/functions.dart';
import '../../domain/repository/repository.dart';
import '../../domain/usecase/register_usecase.dart';
import '../widget/dialog/image_dialog.dart';
import '../widget/dropdown_controllers/cities_controller.dart';
import '../widget/dropdown_controllers/countries_controller.dart';

class RegisterController extends GetxController {
  CountriesController countriesController = Get.find();
  CitiesController citiesController = Get.find();
  late final RegisterUseCase _RegisterUseCase;

  final ImagePicker _imagePicker = ImagePicker();

  final RxString _base64string = "".obs;

  String get base64string => _base64string.value;

  set base64string(String value) {
    _base64string.value = value;
  }

  Rx<File> _image = File("").obs;

  File get image => _image.value;

  set image(File value) {
    _image = value.obs;
  }

  @override
  void onInit() {
    super.onInit();
  }

  RegisterController(Repository repository) {
    _RegisterUseCase = RegisterUseCase(repository);
  }

  final RxString _name = "".obs;

  final RxInt _age = (1).obs;

  final RxString _phone = "".obs;

  final RxInt _gender = (1).obs;

  final RxString _email = "".obs;

  final RxString _password = "".obs;

  final RxString _reenterPassword = "".obs;

  final RxBool _isNameValid = true.obs;

  final RxBool _isAgeValid = true.obs;

  final RxBool _isPhoneValid = true.obs;

  final RxBool _isEmailValid = true.obs;

  final RxBool _isPasswordValid = true.obs;

  final RxBool _isReenterPasswordValid = true.obs;

  final RxBool _startRegister = false.obs;

  final RxBool _isUserRegisterSucssfuly = false.obs;

  final RxBool _showPassword = false.obs;

  final RxBool _showConfirmPassword = false.obs;

  int get age => _age.value;

  String get phone => _phone.value;

  int get gender => _gender.value;

  String get email => _email.value;

  String get password => _password.value;

  bool get isAgeValid => _isAgeValid.value;

  bool get isPhoneValid => _isPhoneValid.value;

  bool get isEmailValid => _isEmailValid.value;

  bool get isNameValid => _isNameValid.value;

  bool get isPasswordValid => _isPasswordValid.value;

  bool get isReenterPasswordValid => _isReenterPasswordValid.value;

  bool get isShowPassword => _showPassword.value;

  bool get showConfirmPassword => _showConfirmPassword.value;

  bool get startRegister => _startRegister.value;

  bool get isUserRegisterSuccessfully => _isUserRegisterSucssfuly.value;

  void setAge(int value) {
    _age.value = value;
  }

  void setPhone(String value) {
    _phone.value = value;
  }

  set gender(int value) {
    _gender.value = value;
  }

  void setName(String value) {
    _name.value = value;
  }

  void setEmail(String value) {
    _email.value = value;
  }

  void setPassword(String value) {
    _password.value = value;
  }

  void setReenterPassword(String value) {
    _reenterPassword.value = value;
  }

  void setShowPassword() {
    _showPassword.value = !_showPassword.value;
  }

  void setShowConfirmPassword() {
    _showConfirmPassword.value = !_showConfirmPassword.value;
  }

  bool _nameValid() {
    return (_name.isNotEmpty);
  }

  bool _ageValid() {
    return (_age.isNaN);
  }

  bool _phoneValid() {
    return (_phone.isEmpty);
  }

  bool _emailValid() {
    return (_email.isNotEmpty && isEmail_Valid(_email.value));
  }

  bool _passwordValid() {
    return (_password.isNotEmpty && isPassword_Valid(_password.value));
  }

  bool _confirmPasswordValid() {
    return (_reenterPassword.isNotEmpty &&
        isConfirmPassword_Valid(_password.value, _reenterPassword.value));
  }

  bool _isAllValid() {
    _isNameValid.value = _nameValid();
    _isAgeValid.value = _ageValid();
    _isPhoneValid.value = _phoneValid();
    _isEmailValid.value = _emailValid();
    _isPasswordValid.value = _passwordValid();
    _isReenterPasswordValid.value = _confirmPasswordValid();
    return _isEmailValid.value &&
        _isPasswordValid.value &&
        _isNameValid.value &&
        _isReenterPasswordValid.value;
  }

  register() async {
    if (_isAllValid()) {
      _startRegister.value = true;
      (await _RegisterUseCase.execute(RegisterUseCaseInput(
              _name.value,
              _gender.value,
              _age.value,
              countriesController.selectedCountry,
              citiesController.selectedCity,
              _phone.value,
              _email.value,
              _password.value,
              _image.value.path)))
          // _base64string.value)))
          .fold(
              (failure) => {
                    // left -> failure
                    _startRegister.value = false,
                    //this.failure = failure,
                    Fluttertoast.showToast(
                        msg: "failure:${failure.message}${failure.code}",
                        toastLength: Toast.LENGTH_LONG)
                  }, (data) {
        // right -> data (success)
        // content
        print("data:${data}");
        // navigate to main screen
        _startRegister.value = false;
        _isUserRegisterSucssfuly.value = true;
        Fluttertoast.showToast(
            msg: "  Success Registration ", toastLength: Toast.LENGTH_LONG);
      });
    } else {
      print("NOT VALID");
    }
  }

  final RxBool _isChecked = false.obs;

  bool get isChecked => _isChecked.value;

  set isChecked(bool value) {
    _isChecked.value = value;
  }

  final RxList<String> _genderItems = [
    'Male',
    'Female',
  ].obs;

  List<String> get genderItems => _genderItems;

  final RxString _selectGender = "Male".obs;

  String get selectGender => _selectGender.value;

  set selectGender(String value) {
    _selectGender.value = value;
    value == "Male" ? _gender.value = 0 : _gender.value = 1;
  }

  setProfilePicture(File file) {}

  openImageDialog() {
    Get.dialog(ImageDialog());
  }

  updateImage(String source) async {
    print("update clicked");
    bool granted = false;
    switch (source) {
      case 'camera':
        if (await Permission.camera.status.isDenied) {
          if (await Permission.camera.request().isGranted) granted = true;
        } else {
          granted = true;
        }
        if (granted) {
          XFile? pickedFile =
              await _imagePicker.pickImage(source: ImageSource.camera);
          if (pickedFile != null) {
            image = File(pickedFile.path);
            print(image);

            File imagefile = File(pickedFile.path); //convert Path to File
            Uint8List imagebytes =
                await imagefile.readAsBytes(); //convert to bytes
            _base64string.value =
                base64.encode(imagebytes); //convert bytes to base64 string
          }
        }
        Get.back();
        break;
      case 'gallery':
        if (await Permission.storage.isDenied) {
          if (await Permission.storage.request().isGranted) granted = true;
        } else {
          granted = true;
        }
        if (granted) {
          XFile? pickedFile =
              await _imagePicker.pickImage(source: ImageSource.gallery);
          if (pickedFile != null) {
            image = File(pickedFile.path);
          }
        }
        Get.back();
        break;
    }
    update();
  }
}
