import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:job_application_test/data/response/responses.dart';
import 'package:job_application_test/domain/model/models.dart';
import 'package:job_application_test/domain/usecase/getallcitypartners_usecase.dart';

import '../../domain/repository/repository.dart';
import '../widget/dropdown_controllers/cities_controller.dart';
import '../widget/dropdown_controllers/countries_controller.dart';

class SearchController extends GetxController {
  CountriesController countriesController = Get.find();
  CitiesController citiesController = Get.find();
  late final GetAllCityPartnersUseCase _allCityPartnersUseCase;

  SearchController(Repository repository) {
    _allCityPartnersUseCase = GetAllCityPartnersUseCase(repository);
  }

  int? ageto;
  int? agefrom;
  DateTime? date;

  final RxInt _gender = (1).obs;

  int get gender => _gender.value;

  set gender(int value) {
    _gender.value = value;
  }

  final RxList<String> _genderItems = [
    'Male',
    'Female',
  ].obs;

  List<String> get genderItems => _genderItems;

  final RxString _selectGender = "Male".obs;

  String get selectGender => _selectGender.value;

  set selectGender(String value) {
    _selectGender.value = value;
    value == "Male" ? _gender.value = 0 : _gender.value = 1;
  }

  final RxInt _ageFrom = (1).obs;

  int get ageFrom => _ageFrom.value;

  set ageFrom(int value) {
    _ageFrom.value = value;
    agefrom = value;
  }

  final RxInt _ageTo = (1).obs;

  int get ageTo => _ageTo.value;

  set ageTo(int value) {
    _ageTo.value = value;
    ageTo = value;
  }

  final dateController = TextEditingController();

  DateTime _dateTime = DateTime.now();

  DateTime get dateTime => _dateTime;

  set dateTime(DateTime value) {
    _dateTime = value;
    date = value;
  }

  final RxBool _startSearch = false.obs;

  bool get startSearch => _startSearch.value;

  final RxBool _sucSearch = false.obs;

  bool get sucSearch => _sucSearch.value;

  List<GetAllCityPartners> _partners = [];

  List<GetAllCityPartners> get partners => _partners;

  set partners(List<GetAllCityPartners> value) {
    _partners = value;
  }

  getPartners() async {
    _startSearch.value = true;
    (await _allCityPartnersUseCase.execute(GetAllCityPartnersUseCaseInput(
            citiesController.selectedCity,
            countriesController.selectedCountry,
            _gender.value,
            ageto,
            agefrom,
            date,
            "",
            "")))
        .fold(
            (failure) => {
                  _startSearch.value = false,
                  Fluttertoast.showToast(
                      msg: failure.message!, toastLength: Toast.LENGTH_LONG)
                }, (List<GetAllCityPartners> data) {
      partners = data;
      _startSearch.value = false;
      _sucSearch.value = true;
      // right -> data (success)
      // content
    });
  }
}
