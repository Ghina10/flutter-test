import 'package:get/get.dart';
import 'package:job_application_test/view/search/search_controller.dart';

import '../../app/di.dart';

class SearchBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SearchController(NoteAppDependency().repo));
  }
}
