import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/resources/routes_manager.dart';
import 'package:job_application_test/view/search/search_controller.dart';

import '../resources/strings_manager.dart';
import '../resources/value_manager.dart';
import '../responsive/media_query.dart';
import '../widget/cities_drop_down.dart';
import '../widget/countries_drop_down.dart';

class SearchView extends GetView<SearchController> {
  SearchView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.findpartner),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: AppSize.s28),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p12),
              child: buildCountriesDropDown(),
            ),
            const SizedBox(height: AppSize.s12),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p12),
              child: buildCitiesDropDown(),
            ),
            const SizedBox(height: AppSize.s12),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p12),
              child: buildGenderDropDown(),
            ),
            const SizedBox(height: AppSize.s12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p18),
              child: Row(
                children: const [
                  Text(AppStrings.partnerAge),
                ],
              ),
            ),
            const SizedBox(height: AppSize.s12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p18),
              child: Row(
                children: [
                  const Expanded(child: Text(AppStrings.between)),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppPadding.p18),
                      child: TextFormField(
                        //controller: _emailController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          hintText: AppStrings.age,
                        ),
                        onChanged: (agefrom) =>
                            controller.ageFrom = int.parse(agefrom),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: AppSize.s12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p18),
              child: Row(
                children: [
                  const Expanded(child: Text(AppStrings.and)),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppPadding.p18),
                      child: TextFormField(
                        //controller: _emailController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          hintText: AppStrings.age,
                        ),
                        onChanged: (ageto) =>
                            controller.ageTo = int.parse(ageto),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: AppSize.s12),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: AppPadding.p18),
              child: TextField(
                readOnly: true,
                controller: controller.dateController,
                decoration: const InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: AppPadding.p18),
                    hintText: AppStrings.date),
                onTap: () async {
                  DateTime? date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2100));
                  controller.dateController.text =
                      date.toString(); //.substring(0,10);
                },
              ),
            ),
            const SizedBox(height: AppSize.s12),
            Obx(
              () => controller.startSearch
                  ? const Center(child: CircularProgressIndicator())
                  : ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: Size(getWidth(context) * 0.90, AppSize.s50),
                      ),
                      onPressed: () async {
                        controller.getPartners();
                        controller.sucSearch
                            ? Get.toNamed(Routes.suggestionRoute,
                                arguments: [controller.partners])
                            : Container();
                      },
                      child: Text(
                        AppStrings.search,
                        style: Theme.of(context).textTheme.displayLarge,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCountriesDropDown() {
    return const CountriesDropDown();
  }

  Widget buildCitiesDropDown() {
    return const CitiesDropDown();
  }

  Widget buildGenderDropDown() {
    return InputDecorator(
      decoration: const InputDecoration(
        hintText: AppStrings.gender,
      ),
      child: Obx(() {
        return DropdownButtonHideUnderline(
          child: DropdownButton2<String>(
              items: controller.genderItems
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              isExpanded: true,
              isDense: true,
              onChanged: (String? newSelectedGender) {
                controller.selectGender = newSelectedGender!;
              },
              value: controller.selectGender),
        );
      }),
    );
  }
}
