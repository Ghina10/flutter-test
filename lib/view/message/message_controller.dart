import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:job_application_test/domain/model/models.dart';
import 'package:job_application_test/domain/usecase/getallchatlist_usecase.dart';
import 'package:job_application_test/view/widget/get_imag_controller/get_image_controller.dart';

import '../../domain/repository/repository.dart';

class MessageController extends GetxController {
  final imageController = Get.find<ImageController>();
  late final GetAllChatListUseCase _allChatListUseCase;

  MessageController(Repository repository) {
    _allChatListUseCase = GetAllChatListUseCase(repository);
  }

  List<GetAllChatList> _chats = [];

  List<GetAllChatList> get chat => _chats;

  set chat(List<GetAllChatList> value) {
    _chats = value;
  }

  final RxString _name = "".obs;

  String get name => _name.value;

  set name(String value) {
    _name.value = value;
  }

  final RxString _lastmessag = "".obs;

  String get lastMessag => _lastmessag.value;

  set lastMessag(String value) {
    _lastmessag.value = value;
  }

  DateTime _date = DateTime.now();

  DateTime get date => _date;

  set date(DateTime value) {
    _date = value;
  }

  final RxInt _accountid = (1).obs;

  int get accountId => _accountid.value;

  set accountId(int value) {
    _accountid.value = value;
  }

  final RxBool _startChat = false.obs;

  bool get startSearch => _startChat.value;


  getChats() async {
    _startChat.value = true;
    (await _allChatListUseCase.execute(""))
        .fold(
            (failure) => {
              _startChat.value = false,
          Fluttertoast.showToast(
              msg: failure.message!, toastLength: Toast.LENGTH_LONG)
        }, (List<GetAllChatList> data) {
      chat = data;
      _startChat.value = false;
      // right -> data (success)
      // content
    });
  }

}
