import 'package:get/get.dart';
import 'package:job_application_test/view/widget/get_imag_controller/get_image_controller.dart';

import '../../app/di.dart';
import 'message_controller.dart';

class MessageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MessageController(NoteAppDependency().repo));
    //Get.lazyPut(() => ImageController(NoteAppDependency().repo));
  }
}
