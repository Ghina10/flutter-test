import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:job_application_test/view/message/message_controller.dart';
import 'package:job_application_test/view/resources/strings_manager.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/value_manager.dart';

class MessageView extends GetView<MessageController> {
  const MessageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.message),
      ),
      body: controller.chat.isEmpty
          ? buildEmptyScreen(context)
          : ListView.builder(
              itemExtent: 110,
              itemCount: 4,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {},
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.94,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppPadding.p12),
                      color: Colors.white,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Container(
                                  height: AppSize.s70,
                                  width: AppSize.s70,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: AppMargin.m2,
                                      horizontal: AppMargin.m2),
                                  padding: const EdgeInsets.all(AppPadding.p5),
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(AppSize.s12),
                                    border: Border.all(
                                        width: 2, color: ColorManager.black),
                                  ),
                                  child: Image.asset(controller.imageController
                                      .getImage(
                                          controller.chat[index].accountId))),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                  child: SizedBox(
                                    child: Text(controller.name,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 0, 10),
                                  child: SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    child: Text(
                                        controller.chat[index].lastMessage!,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(5, 40, 0, 0),
                                  child: Text(
                                    DateFormat('yyyy-MM-dd').format(controller
                                        .chat[index].lastMessageTime!),
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                ),
                              ],
                            ),
                          ]),
                    ),
                  ),
                );
              }),
    );
  }

  Widget buildEmptyScreen(context) {
    return Center(
      child: Text(
        AppStrings.ooops,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.titleLarge,
      ),
    );
  }
}
