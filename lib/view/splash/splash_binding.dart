
import 'package:get/get.dart';
import 'package:job_application_test/view/splash/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
     Get.put(SplashController());
  }
}
