import 'dart:async';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:job_application_test/app/app_prefs.dart';
import 'package:job_application_test/view/resources/routes_manager.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    // AppPreferences().isOnBoardingScreenViewed().then((value) => Get.off(value
    //     ? () =>
    // AppPreferences().isUserLoggedIn() ? const HomeScreen() : LoginView()
    //     : () => OnBoardingView()));
    startSplashScreen();
    super.onInit();
  }

  void startSplashScreen() async {
    Timer(const Duration(seconds: 2), navigateToNextScreen);
  }

  void navigateToNextScreen() async {
    AppPreferences().isUserLoggedIn()
        ? await Get.toNamed(Routes.homeRoute)
        : await Get.toNamed(Routes.beforeRegisterRoute);
  }
}
