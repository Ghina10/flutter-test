import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/bottom_navigation/pandabar_controller.dart';
import 'package:job_application_test/view/home/home_view.dart';
import 'package:job_application_test/view/message/message_view.dart';
import 'package:job_application_test/view/resources/routes_manager.dart';
import 'package:job_application_test/view/search/search_view.dart';
import 'package:pandabar/pandabar.dart';

import '../resources/strings_manager.dart';

class PandaNavigationBar extends GetView<PandaBarController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: PandaBar(
        buttonColor: Get.theme.shadowColor,
        buttonSelectedColor: Get.theme.primaryColorLight,
        fabColors: [
          controller.isSelected
              ? Get.theme.primaryColorLight
              : Get.theme.shadowColor,
          controller.isSelected
              ? Get.theme.primaryColorLight
              : Get.theme.shadowColor,
          controller.isSelected
              ? Get.theme.primaryColorLight
              : Get.theme.shadowColor,
        ],
        fabIcon: const Icon(
          Icons.search,
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        buttonData: [
          PandaBarButtonData(
            id: 'home',
            icon: Icons.home,
            title: AppStrings.home,
          ),
          PandaBarButtonData(
            id: 'message',
            icon: Icons.message,
            title: AppStrings.message,
          ),
        ],
        onChange: (id) {
          controller.pageId = id;
        },
        onFabButtonPressed: (id) {
          controller.isSelected = true;
          controller.pageId = id;
        },
      ),
      body: Obx(() => BuildBody()),
    );
  }

  Widget BuildBody() {
    switch (controller.pageId) {
      case 'home':
        //print("home");
        controller.isSelected = false;
        return const HomeView();
      case 'message':
       // print("message");
        controller.isSelected = false;
        return const MessageView();
      case 'search':
       // print("search");
        controller.isSelected = true;
        return SearchView();
      default:
        return Container();
    }
  }
}
