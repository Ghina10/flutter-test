import 'package:get/get.dart';

class PandaBarController extends GetxController {
// late final RegisterUseCase _RegisterUseCase;

  final RxString _pageId = 'home'.obs;

  String get pageId => _pageId.value;

  set pageId(String value) {
    _pageId.value = value;
  }

  final RxBool _isSelected = false.obs;

  bool get isSelected => _isSelected.value;

  set isSelected(bool value) {
    _isSelected.value = value;
  }
}
