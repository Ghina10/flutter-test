import 'package:get/get.dart';
import 'package:job_application_test/view/bottom_navigation/pandabar_controller.dart';
import 'package:job_application_test/view/home/home_controller.dart';
import 'package:job_application_test/view/message/message_controller.dart';
import 'package:job_application_test/view/search/search_controller.dart';

import '../../app/di.dart';
import '../widget/dropdown_controllers/cities_controller.dart';
import '../widget/dropdown_controllers/countries_controller.dart';
import '../widget/get_imag_controller/get_image_controller.dart';

class PandaBarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PandaBarController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => SearchController(NoteAppDependency().repo));
    Get.lazyPut(() => MessageController(NoteAppDependency().repo));
    Get.lazyPut(() => CountriesController(NoteAppDependency().repo));
    Get.lazyPut(() => CitiesController(NoteAppDependency().repo));
    Get.lazyPut(() => ImageController(NoteAppDependency().repo));
  }
}
