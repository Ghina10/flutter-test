import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../domain/repository/repository.dart';
import '../../../domain/usecase/downloadimage_usecase.dart';


class ImageController extends GetxController{
  late final DownloadImageUseCase _downloadImageUseCase;

  ImageController(Repository repository) {
    _downloadImageUseCase = DownloadImageUseCase(repository);
  }

  String? id;

  final RxBool _startimage = false.obs;
  bool get startimage => _startimage.value;

  final RxBool _suciamge = false.obs;
  bool get sucimage => _suciamge.value;


  getImage(int? id) async {
    _startimage.value = true;
    (await _downloadImageUseCase.execute(id!))
        .fold(
            (failure) => {
          _startimage.value = false,
          Fluttertoast.showToast(
              msg: "failure:${failure.message}${failure.code}",
              toastLength: Toast.LENGTH_LONG)
        }, (data) {
      // right -> data (success)
      // content
      print("data:${data}");
      // navigate to main screen
      _startimage.value = false;
      _suciamge.value = true;
    });
  }


}