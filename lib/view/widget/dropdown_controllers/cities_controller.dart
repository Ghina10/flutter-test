import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:job_application_test/domain/usecase/getallcity_usecase.dart';

import '../../../domain/model/models.dart';
import '../../../domain/repository/repository.dart';
import 'countries_controller.dart';

class CitiesController extends GetxController {
  late final GetAllCityUseCase _allCityUseCase;
  CountriesController countriesController = Get.find();

  CitiesController(Repository repository) {
    _allCityUseCase = GetAllCityUseCase(repository);
  }

  final RxBool _startloading = false.obs;

  set startloading(bool value) {
    _startloading.value = value;
  }

  bool get startloading => _startloading.value;

  final RxList<DropdownMenuItem<String>> _items =
      <DropdownMenuItem<String>>[].obs;

  List<DropdownMenuItem<String>>? get items => _items.value;

  final RxInt _selectedCity = 1.obs;

  int get selectedCity => _selectedCity.value;

  set selectedCity(int value) {
    _selectedCity.value = value;
  }

  initItemList() async {
    startloading = true;
    List<City> _city = [];
    (await _allCityUseCase.execute("")).fold((failure) {
      startloading = false;
      return Fluttertoast.showToast(
          msg: "${failure.message}", toastLength: Toast.LENGTH_LONG);
    }, (data) async {
      _city = data;
      startloading = false;
    });
    if (_city.isEmpty) {
      _items.value = [];
    } else {
      for (int i = 0; i < _city.length; i++) {
        selectedCity = _city[0].id;
       // if (countriesController.selectedCountry == _city[i].id.toString()) {
          _items.value.add(
            DropdownMenuItem<String>(
              alignment: AlignmentDirectional.centerEnd,
              child: Text(_city[i].name),
              value: _city[i].id.toString(),
            ),
          );
        //}
      }
    }
  }

  @override
  void onInit() {
    initItemList();
  }
}
