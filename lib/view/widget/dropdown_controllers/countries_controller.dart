import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:job_application_test/domain/usecase/getallcountry_usecase.dart';

import '../../../domain/model/models.dart';
import '../../../domain/repository/repository.dart';

class CountriesController extends GetxController {
  late final GetAllCountryUseCase _allCountryUseCase;

  CountriesController(Repository repository) {
    _allCountryUseCase = GetAllCountryUseCase(repository);
  }

  final RxBool _startloading = false.obs;

  set startloading(bool value) {
    _startloading.value = value;
  }

  bool get startloading => _startloading.value;

  final RxList<DropdownMenuItem<String>> _items =
      <DropdownMenuItem<String>>[].obs;

  List<DropdownMenuItem<String>>? get items => _items.value;

  final RxInt _selectedCountry = 1.obs;

  int get selectedCountry => _selectedCountry.value;

  set selectedCountry(int value) {
    _selectedCountry.value = value;
    //print('_selectedCountry $selectedCountry');
  }

  initItemList() async {
    startloading = true;
    List<Country> _countries = [];
    (await _allCountryUseCase.execute("")).fold((failure) {
      startloading = false;
      return Fluttertoast.showToast(
          msg: "${failure.message}", toastLength: Toast.LENGTH_LONG);
    }, (data) async {
      _countries = data;
      startloading = false;
    });
    if (_countries.isEmpty) {
      _items.value = [];
    } else {
      for (int i = 0; i < _countries.length; i++) {
        selectedCountry = _countries[0].city[0].id;
        _items.value.add(
          DropdownMenuItem<String>(
            alignment: AlignmentDirectional.centerEnd,
            child: Text(_countries[i].name),
            value: _countries[i].city[i].id.toString(),
          ),
        );
      }
    }
  }

  @override
  void onInit() {
    initItemList();
  }
}
