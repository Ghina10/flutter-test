import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/resources/color_manager.dart';

import '../resources/font_manager.dart';
import '../resources/strings_manager.dart';
import 'dropdown_controllers/cities_controller.dart';

class CitiesDropDown extends GetView<CitiesController> {

  const CitiesDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: FormField<String>(
        builder: (FormFieldState<String> state) {
          return InputDecorator(
            decoration: const InputDecoration(
                hintText: AppStrings.city,
            ),
            child: Obx(() {
              return DropdownButtonHideUnderline(
                child: DropdownButton2<String>(
                    style: const TextStyle(
                      fontSize: 13,
                      color: Colors.black,
                    ),
                    hint: Text(
                      AppStrings.city,
                      style: TextStyle(
                          color: ColorManager.grey,
                          fontSize: FontSize.s16,
                          fontFamily: FontConstants.fontFamily,
                          fontWeight: FontWeightManager.semiBold),
                    ),
                    items: controller.items,
                    isExpanded: true,
                    isDense: true,
                    onChanged: (String? newSelectedCity) {
                      controller.selectedCity = int.parse(newSelectedCity!);
                    },
                    value: controller.selectedCity.toString()),
              );
            }),
          );
        },
      ),
    );
  }
}
