import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/resources/color_manager.dart';

import '../resources/font_manager.dart';
import '../resources/strings_manager.dart';
import 'dropdown_controllers/countries_controller.dart';

class CountriesDropDown extends GetView<CountriesController> {
  const CountriesDropDown({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: FormField<String>(
        builder: (FormFieldState<String> state) {
          return InputDecorator(
            decoration: const InputDecoration(
              hintText: AppStrings.country,
            ),
            child: Obx(() {
              return DropdownButtonHideUnderline(
                child: DropdownButton2<String>(
                    style: const TextStyle(
                      fontSize: 13,
                      color: Colors.black,
                    ),
                    hint: Text(
                      AppStrings.country,
                      style: TextStyle(
                          color: ColorManager.grey,
                          fontSize: FontSize.s16,
                          fontFamily: FontConstants.fontFamily,
                          fontWeight: FontWeightManager.semiBold),
                    ),
                    items: controller.items,
                    isExpanded: true,
                    isDense: true,
                    onChanged: (String? newSelectedCountry) {
                      controller.selectedCountry =
                          int.parse(newSelectedCountry!);
                    },
                    value: controller.selectedCountry.toString()),
              );
            }),
          );
        },
      ),
    );
  }
}
