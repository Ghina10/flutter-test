import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:job_application_test/view/register/register_controller.dart';

import '../../resources/color_manager.dart';
import '../../resources/strings_manager.dart';

class ImageDialog extends GetView<RegisterController> {


  ImageDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        AppStrings.choose,
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      content: Padding(
        padding: const EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: ListBody(
            children: [
              const Divider(),
              ListTile(
                onTap: () {
                  controller.updateImage("gallery");
                },
                title: const Text(AppStrings.gallery),
                leading: const Icon(
                  Icons.image_outlined,
                ),
              ),
              const Divider(),
              ListTile(
                onTap: () {
                 controller.updateImage("camera");
                },
                title: const Text(AppStrings.camera),
                leading: const Icon(
                  Icons.camera_alt_outlined,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
