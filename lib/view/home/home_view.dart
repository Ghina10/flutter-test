import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_application_test/view/home/home_controller.dart';
import 'package:job_application_test/view/resources/strings_manager.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../resources/assets_manager.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          AppStrings.home,
        ),
      ),
      body: Stack(
        children: [
          buildBackground(),
        ],
      ),
    );
  }

  Widget buildBackground() {
    return Center(
      child: SvgPicture.asset(ImageAssets.welcome),
    );
  }
}
