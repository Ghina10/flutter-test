import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../resources/assets_manager.dart';
import '../resources/color_manager.dart';
import '../resources/routes_manager.dart';
import '../resources/strings_manager.dart';
import '../resources/value_manager.dart';
import '../responsive/media_query.dart';
import 'login_controller.dart';

class LoginView extends GetView<LoginController> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  LoginView({Key? key}) : super(key: key);

  //final get = Get.lazyPut(() => LoginViewModel(NoteAppDependency().repo), fenix: true);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            buildBackground(),
            buildCard(context),
          ],
        ),
      ),
    );
  }

  Widget buildCard(context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: getHeight(context),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(height: getHeight(context) * 0.05),
                Expanded(
                  flex: 1,
                  child: Text(
                    AppStrings.login,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Center(child: buildText(context)),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Get.offNamed(Routes.registerRoute);
                          },
                          child: Row(
                            children: [
                              Text(AppStrings.creatAccount,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineSmall),
                              Text(AppStrings.signUp,
                                  style:
                                      Theme.of(context).textTheme.displaySmall),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: getHeight(context) * 0.05),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBackground() {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(ImageAssets.splash),
          fit: BoxFit.none,
        ),
      ),
    );
  }

  Widget buildText(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppPadding.p8),
      child: Container(
        color: Colors.transparent,
        child: Form(
          key: _formkey,
          child: Padding(
            padding: const EdgeInsets.symmetric(
                vertical: AppPadding.p12, horizontal: AppPadding.p15),
            child: Column(
              children: [
                // const SizedBox(height: AppSize.s35),
                Obx(
                  () => Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: AppPadding.p18),
                    child: TextFormField(
                      //controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: buildInputDecoration(
                          Icons.email, AppStrings.email, controller, true),
                      onChanged: (email) => controller.setEmail(email),
                    ),
                  ),
                ),
                const SizedBox(height: AppSize.s28),
                Obx(
                  () => Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: AppPadding.p18),
                    child: TextFormField(
                      obscureText: !controller.showPassword,
                      //controller: _PasswordController,
                      keyboardType: TextInputType.text,
                      decoration: buildInputDecoration(
                          Icons.lock, AppStrings.password, controller, false),
                      onChanged: (password) => controller.setPassword(password),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Obx(
                      () => Checkbox(
                        value: controller.isChecked,
                        onChanged: (bool? value) {
                          controller.isChecked = value!;
                        },
                      ),
                    ),
                    Obx(
                      () => controller.isChecked
                          ? Text(AppStrings.remember,
                              style: Theme.of(context).textTheme.bodySmall)
                          : Text(AppStrings.remember,
                              style: Theme.of(context).textTheme.titleSmall),
                    ),
                  ],
                ),
                const SizedBox(height: AppSize.s35),
                Obx(
                  () => controller.startLogin
                      ? const Center(child: CircularProgressIndicator())
                      : ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            fixedSize: Size(
                                getWidth(context) -
                                    AppSize.s100,
                                AppSize.s50),
                          ),
                          onPressed: () async {
                            await controller.login();
                            controller.isUserLoginSucssfuly
                                ? Get.offAllNamed(Routes.homeRoute)
                                : () {};
                            //Get.offNamed(Routes.homeRoute);
                          },
                          child: Text(
                            AppStrings.login,
                            style: Theme.of(context).textTheme.displayLarge,
                          ),
                        ),
                ),
                // const SizedBox(height: AppSize.s35),
              ],
            ),
          ),
        ),
      ),
    );
  }

  InputDecoration buildInputDecoration(IconData icons, String hinttext,
      LoginController loginController, bool isEmailField) {
    //print(valid);
    //print(isEmailField);
    return InputDecoration(
        hintText: hinttext,
        errorText: isEmailField
            ? loginController.isEmail_valid
                ? null
                : AppStrings.invalidEmail
            : loginController.isPasswordValid
                ? null
                : AppStrings.passwordInvalid,
        suffixIcon: !isEmailField
            ? GestureDetector(
                onTap: () {
                  loginController.setShowPassword();
                },
                child: Icon(
                  loginController.showPassword
                      ? Icons.visibility
                      : Icons.visibility_off,
                  color: ColorManager.grey,
                  size: 22,
                ),
              )
            : const Icon(Icons.email));
  }
}
