import 'package:get/get.dart';

import '../../app/di.dart';
import 'login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController(NoteAppDependency().repo));
  }
}
