import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../app/app_prefs.dart';
import '../../app/functions.dart';
import '../../domain/repository/repository.dart';
import '../../domain/usecase/login_usecase.dart';
import '../resources/routes_manager.dart';

class LoginController extends GetxController {
  LoginController(Repository repository) {
    _loginUseCase = LoginUseCase(repository);
  }

  late final LoginUseCase _loginUseCase;

  final RxString _email = "".obs;

  final RxString _password = "".obs;

  final RxBool _isEmailValid = true.obs;

  final RxBool _showPassword = false.obs;

  final RxBool _isPasswordValid = true.obs;

  final RxBool _isUserLoginSucssfuly = false.obs;

  final RxBool _startLogin = false.obs;

  // Failure failure = Failure.Default();

  void setEmail(String value) {
    _email.value = value;
    //  _isAllValid();
  }

  void setPassword(String value) {
    _password.value = value;
    //  _isAllValid();
  }

  setShowPassword() {
    _showPassword.value = !_showPassword.value;
  }

  bool get isUserLoginSucssfuly => _isUserLoginSucssfuly.value;

  bool get startLogin => _startLogin.value;

  String get email => _email.value;

  String get password => _password.value;

  bool get isEmail_valid => _isEmailValid.value;

  bool get isPasswordValid => _isPasswordValid.value;

  bool get showPassword => _showPassword.value;

  bool _isAllValid() {
    _isEmailValid.value = _emailValid();
    _isPasswordValid.value = _passwordValid();
    // print("_isPasswordValid$_isPasswordValid");print("_isEmailValid$_isEmailValid");
    return _isEmailValid.value && _isPasswordValid.value;
  }

  bool _emailValid() {
    return (_email.isNotEmpty && isEmail_Valid(_email.value));
  }

  bool _passwordValid() {
    //print(_password.isNotEmpty);
    return (_password.isNotEmpty);
  }

  login() async {
    //  print(AppPreferences().getToken().then((value) => print(value)));
    if (_isAllValid()) {
      _startLogin.value = true;
      (await _loginUseCase.execute(LoginUseCaseInput(
              _email.value, _password.value, _isChecked.value)))
          .fold(
              (failure) => {
                    _startLogin.value = false,
                    Fluttertoast.showToast(
                        msg: failure.message!, toastLength: Toast.LENGTH_LONG)
                  }, (data) {
        // right -> data (success)
        // content
        Fluttertoast.showToast(
            msg: "Login Success", toastLength: Toast.LENGTH_LONG);

        AppPreferences().setUserLoggedIn();
        // navigate to main screen
        _startLogin.value = false;
        _isUserLoginSucssfuly.value = true;
      });
    } else {
      //Todo:show dialog error
      print("NOT VALID");
    }
  }

  final RxBool _isChecked = false.obs;

  bool get isChecked => _isChecked.value;

  set isChecked(bool value) {
    _isChecked.value = value;
  }
}
